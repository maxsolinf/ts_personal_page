FROM linux/arm64/v8/node
WORKDIR /app
COPY ./ /app
CMD yarn install
CMD yarn global add serve
CMD yarn build
ENTRYPOINT ["serve", "build"]