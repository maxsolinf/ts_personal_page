import Layout from './pages/Layout';
import {MessageProvider} from "./contexts/MessagerContext"


function App() {
  return (
    <>
    <MessageProvider>
      <Layout />
    </MessageProvider>
    </>
  );
}

export default App;
