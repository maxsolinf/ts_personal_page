import { Link } from "react-router-dom";
import styled from "styled-components";

export const FooterButton = styled.button`
    color: ${props=> props.color};
    display:inline-block;
    border-radius: 50%;
    width:2em;
    height:2em;

    &>svg{
        width:2rem;
        height:2rem;
    }
    &:not(:first-child){
        margin-left: 4rem;
    }
`
export const FooterNavControl = styled.nav`
@media only screen and (max-width: 600px){
    display:none;
}
    display: flex;
    flex-flow: row wrap;
    grid-area: footer;
    align-self: end;
    justify-content: center;
    position: sticky;
    bottom:0;
    background:white;
    ;      
`
export const NavBarButton = styled(Link)`
@media only screen and (max-width: 600px){

}
@media only screen and (min-width: 600px){
    padding: 0.25rem;
    border-radius: 10px;;
    transition: ease 350ms;
    text-decoration: none;
    color: black;
}
    align-self: stretch;
    filter: grayscale(10%) opacity(90%) brightness(0.8);
    &:hover{
        background: #f0f0f0;
        filter: grayscale(0%) opacity(100%) brightness(1)
    }
    &>svg{
        height: 2rem;
        width: 2rem;
    }
    & span{
        text-transform: capitalize;
    }

`
export const NavbarControl = styled.nav`
@media only screen and (max-width: 600px){
    flex-flow: row-nowrap;
    justify-content: space-evenly;
    align-self:end;
    align-content:stretch;
    overflow-x:auto;
    &>a>span{
        display:none;
    }
    position:sticky;
    bottom:0;
    background: white;
    z-index:1;
   
    
}
@media only screen and (min-width: 600px){
    height: calc(100vh - 8rem);
    flex-flow: column nowrap;
    &>a{
        display:flex;
        align-items:center;
        gap:16px;

    }
    position:sticky;
    top:4rem;
    left:3px;
    overflow-y:auto;
    
}
    display:flex;
    grid-area: navbar;
    overflow-y:hidden;
`
export const TopbarControl = styled.nav`
grid-area: topbar;
display:flex;
justify-content: space-between;
position:sticky;
top:0;
&>a{
    margin-left: 10px;
    margin-right: 10px;
}
`
export const TopbarButton = styled(Link)`

`

