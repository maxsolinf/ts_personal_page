import styled from "styled-components";

export const SpinerS = styled.div`
    & {
        border: 10px solid rgba(0, 0, 0, .1);
        border-radius: 50%;
        width: 70px;
        height:70px;
        border-left-color: #09f;

        animation: spin 1s ease infinite;
    }
    @keyframes spin{
        0% {
            transform: rotate(0deg);
        }
        100%{
            transform: rotate(360deg);
        }
    }
`