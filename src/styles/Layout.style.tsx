import styled from 'styled-components'

export const AppDiv = styled.div`
@media only screen and (max-width: 600px){
    grid-template-areas:
    "topbar"
    "main"
    "navbar";
    grid-template-columns: 1fr;
    grid-template-rows: 2rem 1fr 3rem;
}

@media only screen and (min-width: 600px){
    grid-template-areas:
    "topbar topbar topbar topbar"
    "navbar main main empty"
    "footer footer footer footer";
    grid-template-columns: 14rem 1fr 1fr 14rem;
    grid-template-rows: 3rem 1fr 3rem;
    overflow-y:hidden;
}
& {
    display: grid;
    margin: 0;
    padding: 0;
}
`;