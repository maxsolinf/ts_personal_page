import styled from 'styled-components';

export const MapContainerS = styled.section`
@media only screen and (max-width: 600px){
    &{
        width: 97vw;
        height: 65vh;   
    }
}
@media only screen and (min-width: 600px){
    & {
        height: 74vh;
        width: 60vw;
    }
}
@media only screen and (min-width: 1563px){
    & {
        min-height: 50vh;
        max-height: 70vh;
        width: 60vw;
    }

}

&  div.esri-ui{
    display: none;
}

`