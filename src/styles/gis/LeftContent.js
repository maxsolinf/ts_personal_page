import styled from "styled-components";

export const LeftbarS = styled.section`
@media only screen and (max-width: 600px){}
@media only screen and (min-width: 600px){
    & {
        height: 74vh;
        width: 18vw;
        border: 0.3px solid grey;
        overflow-y: auto;
    }
}
@media only screen and (min-width: 1563px){
    & {
        height: 74vh;
        width: 20vw;
        border: 0.3px solid grey;
        overflow-y: auto;
    }

}
    & {
       
    }
`
export const PointStorage = styled.div`
    & {
        display: flex;
        overflow-y: auto;
        flex-flow: row wrap;
        max-height: 4rem;
        min-height: 1rem;
        border: 1px solid black;
    }
    & >p.point{
        background-color:hsla(200, 50%, 50%, 0.7);
        color: white;
        border-radius: 5px;
        width: 25px;
        gap: 1rem;
        margin: 2px 3px;
        text-align: center;
        padding: 2.5    px;
    }
`
export const ControllerS = styled.div`
@media only screen and (max-width: 600px){
    &{
        right:20%;
        top:43%;
        left: 65%;
    }
}
@media only screen and (min-width: 600px){
    & {
        right:0%;
        top:15%;
        left: 83%;
    }
}
@media only screen and (min-width: 1563px){
    & {
        left:88%;
        top:15%;
        
    }
}


    & {
        display: grid;
        grid-template-areas:
        "upleft up upright zoomin"
        "left center right empty"
        "downleft down downright zoomout";
        width: 7rem;
        height:6rem;
        position: absolute;
       
        
    }
    & > svg{
        width: 1.5rem;
        height: 1.5rem;
    }
    .zoomin{
        grid-area: zoomin;
        margin-left: 1.5rem;
    }
    .zoomout{
        grid-area: zoomout;
        margin-left: 1.5rem;
    }
    .upleft{
        grid-area: upleft;
    }
    .up{
        grid-area: up;
    }
    .upright{
        grid-area: upright;
    }
    .left{
        grid-area: left;
    }
    .right{
        grid-area:right;
    }
    .downright{
        grid-area: downright
    }
    .down{
        grid-area: down;
    }
    .downleft{
        grid-area: downleft;
    }

`