import styled from 'styled-components'

export const GitWraper = styled.div`
@media only screen and (max-width: 600px){
    height:calc(100vh - 6rem);
    grid-template-areas: 
    "empty1 info empty2"
    "empty3 control empty4"
    "empty5 repo empty6"
    ;
    grid-template-columns: 0.4rem 1fr 0.4rem;
    grid-template-rows: 0.6fr 2.5rem 1fr;

    /* Info */
    & div.github.info{
        max-height:300px;
        min-width: 350px;

    }
    & div.github.info h1, h2{
        padding:0;
        margin:0;
        font-size: 1rem;
    }

    /* Controller */
    & div.github.controller{
        max-height:30px;
    }
    & div.github.controller button{
        width: 1.5rem;
        height: 1.5rem;
    }
    & div.github.controller button > svg{
        width: 1.5rem;
        height: 1.5rem;
    }
    & div.github.controller span{
        text-transform: uppercase;
        font-weight: 700;
        font-size: 1rem;
    }


    /* Zona de Repo*/
    & div.repo.markdown{
        max-height: 340px;
       
    }
    & div.repo.markdown > *{
        padding-left: 2rem;
    } 

}

@media only screen and (min-width: 600px){
    overflow-y:auto;
    height:calc(100vh - 10rem);
    grid-template-areas: 
    "empty1 info empty2"
    "empty3 control empty4"
    "empty5 repo empty6"
    ;
    grid-template-columns: 0.1fr 1fr 0.1fr;
    grid-template-rows: 0.4fr 3rem 1fr;


    & div.github.controller{

    }
    & div.github.controller button{
        width:2rem;
        height: 2rem;
    }
    & div.github.controller button > svg{
        width:2rem;
        height: 2rem;
    }
    & div.github.controller span{
        text-transform: uppercase;
        font-weight: 700;
        font-size: 1.5rem;
    }
    min-height: 300px;
    /* Zona de Repo*/
    & div.repo.markdown{
        max-height: 350px;

        
    }


}
@media only screen and (min-width: 1563px){
    overflow-y:auto;
    height:calc(100vh - 10rem);
    grid-template-areas: 
    "empty1 info empty2"
    "empty3 control empty4"
    "empty5 repo empty6"
    ;
    grid-template-columns: 0.3fr 1fr 0.3fr;
    grid-template-rows: 0.4fr 4rem 1fr;

    & div.github.controller button{
        width:3rem;
        height: 3rem;
    }

    & div.github.controller button > svg{
        width:3rem;
        height: 3rem;
    }
    & div.github.controller span{
        text-transform: uppercase;
        font-weight: 700;
        font-size: 1.8rem;
    }

    /* Repo styles*/
    & div.github.repo.markdown {
        max-height: 470px;
    }
    

}

/* Zona de info */

& div.github.info{
    grid-area: info;
    display: grid;
    grid-template-areas:
    "izquierdo derecho";
    grid-template-columns: 0.3fr 1fr;
    background: hsla(100, 10%, 81%, 0.3);
    border-radius: 50px;
    box-shadow: 10px 10px 8px #888888;
}
& div.github.info *.izquierdo{
    grid-area: izquierdo;
} 
& div.github.info div.derecho{
    grid-area:derecho;
    display:flex;
    flex-flow: column nowrap;
    gap:0;
}
& div.github.info div.derecho *{
    margin: 7px 0 7px 3rem;

}

& div.github.info *.all{
    grid-area: izquierdo/izquierdo / derecho/derecho;
    align-self: center;
    justify-self: center;
}
& div.github.info div.derecho:first-child{
    text-align: center
}

& div.github.info img{
    width: 150px;
    border-radius: 40%;
}


/* Zona de Controlador */
  
& div.github.controller{
    grid-area: control;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    background: hsla(100, 10%, 81%, 0.3);
    border-radius: 50px;
    box-shadow: 10px 10px 8px #888888;
    margin-top: 1rem;
}
& div.github.controller button{
    border-radius: 50%;
    border-width: 0;
    padding:0;
}




/* Zona de Repo*/
& div.repo.markdown{
    overflow-y:auto;
    grid-area:repo;
    position: relative;
    top:0;
    box-shadow: 10px 10px 8px #888888;
    margin-top: 1rem;
    z-index:0;
    background: hsla(100, 10%, 81%, 0.3);
    border-radius: 10px

}

    display:grid;

`