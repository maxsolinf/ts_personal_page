import styled from 'styled-components';

export const AlertWraper = styled.div`
    border-radius: 7px;
    margin: 3px;
    display:flex;

    & > p{
        padding: 0 3px 0 3px;
        margin:0 2px 0 0;
        display:flex;
        align-items:center;
        justify-items:center;
    }

    & >  button{
        margin: 0;
        padding:0;
        border-width: 0;
        border-radius: 50%;
        display:flex;
        align-items:center;
        justify-items: center;
    }button > svg.alert.svg.close {
        width: 1.5rem;
        height: 1.5rem;
    }

    $ > button > svg.alert.svg.close {
        width: 1.5rem;
        height: 1.5rem;
    }
`