import { Link } from 'react-router-dom';
import styled from 'styled-components';

export const MainRoot = styled.div`
@media only screen and (max-width: 600px){
    grid-template-areas: 
    "noti "
    " main ";
    grid-template-rows: 1.2rem 1fr;
    grid-template-columns: 1fr;
    height: calc(100vh - 5.3rem);
    
}

@media only screen and (min-width: 600px){
    grid-template-areas: 
    " noti "
    " main ";
    grid-template-rows: 3rem 1fr;
    grid-template-columns: 1fr;
    min-width: 600px;
    min-height: 300px
    
}
@media only screen and (min-width: 1563px){
    grid-template-areas: 
    "noti noti noti"
    "empty1 main empty2";
    grid-template-rows: 3rem 1fr;
    grid-template-columns: 5rem 1fr 5rem;
    
}
    &{
        display: grid;
        grid-area: main;

    }
    & *{
        grid-area: main;
        
    }
    & div.main.message.board{
        grid-area: noti;
        align-self: center;
        justify-self: center;
        display:flex;
        flex-flow: column wrap;
        transition: ease 700ms;
        overflow: hidden;
        height: 1.3rem;
    }
`
export const PrjLink = styled(Link)`
    & {
        text-decoration: none;
    }
`
export const PrjCard = styled.div`
@media only screen and (max-width: 600px){
    &{
        width: 95vw;
        height: 50vh;
    }
}
@media only screen and (min-width: 600px){
    &{
        width: 40vw;
        height: 300px
    }
    & > img{
        width: 40vh;
    }
}
@media only screen and (min-width: 1563px){
    &{
        min-width: 350px;
        max-width: 350px;
        min-height: 250px;
    }
    & > img{
        width: 100%;
        height: 100%;
    }
}
&{

    border:1px solid black ;
    margin: 1rem;
}

`
export const PrjContainer =styled.div`
    & {
        display: flex;
        flex-flow: row wrap;
        justify-content: space-evenly;
        overflow-y: auto;
    }
`