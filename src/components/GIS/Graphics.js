import { useEffect, useState } from "react"
import { useInteraction } from "./context/InteractionContext"
import { loadModules } from "esri-loader"
import { useJurisdiccion } from "./context/JurisdiccionContext"
import { useGraphics } from "esri-loader-hooks"

const Draw = (props)=>{

    const [graphic, setGraphic] = useState(null)
    const {points, draw, center, drawPoligon, drawMarker, drawCenter, drawHistoricas, drawResultantes} = useInteraction()
    const {searchResult} = useJurisdiccion()
    const [polygonsHistoricas, setPolygonsHistoricas] = useState()
    const [polygonsResultantes, setPolygonsResultantes] = useState()

    props.view.popup.autoOpenEnabled = true;

    useEffect(()=>{
        if (!drawPoligon) return
        if(!draw && !drawPoligon && !drawMarker)return props.view.graphics.removeAll()
        //cargamos modulos de Esri
        loadModules(['esri/Graphic']).then(([Graphic])=>{
            // Creamos poligono
            props.view.graphics.removeAll()
            let rings = points.map((value, idx)=>{
                return [value[1], value[0]]
            })
            rings.push(rings[0])

            const polygon = {
                type: "polygon",
                    rings: rings
                };
                // Creamos el Fill Simbol para argegar el relleno
                const fillSymbol = {
                    type: "simple-fill", 
                    color: [227, 139, 79, 0.6],
                    outline: { 
                        color: [255, 255, 255],
                        width: 1
                    }
                };
                const graphic = new Graphic({
                    geometry: polygon, 
                    symbol: fillSymbol
                })
                setGraphic(graphic)
                props.view.graphics.add(graphic);
                // console.log(graphic.getAttribute(graphic))
                
            }).catch((err)=>console.log(err))//fin de load module
            //console.log(props.view.graphics)
            return function cleanup(){
                props.view.graphics.remove(graphic)
            }
}, [points,  drawPoligon])//eslint-disable-line 

useEffect(()=>{
    if(!drawMarker)return 
    if(!draw && !drawMarker && !drawPoligon)return props.view.graphics.removeAll()
    loadModules(['esri/Graphic']).then(([Graphics])=>{
        const symbol = {
            type: "simple-marker", 
            color: [186, 19, 40],
            style:"diamond",
            width:1,
          };
        const pointList = points.map(((value, idx)=>({
            geometry: {
                type:"point",
                latitude: value[0],
                longitude: value[1]
            },
            symbol: symbol,
            popupTemplate: {
                title: `Point ${idx + 1}`,
                content: `Punto Agregado por usuario\n Latitude: ${value[0]}\nLongitude: ${value[1]}`
            },
            attributes: {
                name: "Graphic",
                description: `P${idx+1}`
            }
        })))
        //console.log("points",pointList)
        props.view.graphics.removeAll()
        for(let item of pointList){
            const graphic = new Graphics(item)
            setGraphic(graphic)
            props.view.graphics.add(graphic);

        }

    }).catch((err)=>console.log(err))
    return function cleanup(){
        props.view.graphics.remove(graphic)
    }
},[points, drawMarker])//eslint-disable-line

useEffect(()=>{
    if(!drawCenter) return
// if(drawCenter && (drawMarker || drawPoligon))return props.view.graphics.removeAll()
loadModules(['esri/Graphic']).then(([Graphics])=>{
    const symbol = {
        type: "simple-marker", 
        color: [126, 19, 40],
        width:1,
      };
    const centerPoint = {
        geometry: {
            type:"point",
            latitude: center[0],
            longitude: center[1]
        },
        symbol: symbol
    }
    props.view.graphics.removeAll()
    const graphic = new Graphics(centerPoint)
    setGraphic(graphic)
    props.view.graphics.add(graphic)
}).catch(err=>console.log(err))
return function cleanup(){
    props.view.graphics.remove(graphic)}
},[center, drawCenter]) //eslint-disable-line

useEffect(()=>{
    if(!searchResult) return
    if(drawResultantes && !drawResultantes.length && drawHistoricas && !drawHistoricas.length) return props.view.graphics.removeAll()
        const polygonsResultantes = []
        if(drawResultantes){
            for(let items of drawResultantes){
                let {puntos} = items
                let resultantesRing = puntos.map((value, idx)=>{
                    const {x,y} = value
                    return [x,y]
                })
                const geometry = {
                    type: "polygon",
                    rings: resultantesRing
                    };
                    // Creamos el Fill Simbol para argegar el relleno
                    const symbol = {
                        type: "simple-fill", 
                        color: [22, 159, 227, 0.5],
                        outline: { 
                            color: [255, 255, 255],
                            width: 2
                        }    
                    };
                    const popupTemplate = {
                        title: "{Name}",
                        content: [{
                            type: "fields",
                            fieldInfos: [
                                {
                                    fieldName: "Posicional",
                                    label: "Posicional",
                                },
                                {
                                    fieldName: "Exp",
                                    label: "Exp",
                                },
                                {
                                    fieldName: "Municipio",
                                    label: "Municipio",
                                },
                                {
                                    fieldName: "Provincia",
                                    label: "Provincia",
                                },
                               
                                {
                                    fieldName: "Ley",
                                    label: "Ley",
                                },
                                {
                                    fieldName: "Operacion",
                                    label: "Operacion",
                                },
                               
                                {
                                    fieldName: "Area",
                                    label: "Area",
                                },
                            ]
                        }]
                     }
                     const attributes = {
                        Name: items.posicional,
                        Posicional: items.posicional,
                        Description: `Parcela ${items.parcela}`,
                        Provincia: items.provincia,
                        Municipio: items.municipio,
                        Exp: items.numeroExpediente,
                        Operacion: items.operacion,
                        Ley: items.capa,
                        Area: items.area + " m²"
                     }
                     const graphic = {geometry, symbol, attributes, popupTemplate}
                     polygonsResultantes.push(graphic)
                    }
                    //props.view.graphics.addMany(polygonsHistoricas);
                    setPolygonsResultantes(polygonsResultantes)                
        }
},[drawResultantes, drawHistoricas])//eslint-disable-line


useEffect(()=>{
    if(!searchResult) return
    if(drawResultantes && !drawResultantes.length && drawHistoricas && !drawHistoricas.length) return props.view.graphics.removeAll()
        const polygonsHistoricas = []
        if(drawHistoricas){
            for(let items of drawHistoricas){
                let {puntos} = items
                let HistoricasRing = puntos.map((value, idx)=>{
                    const {x,y} = value
                    return [x,y]
                })
                const geometry = {
                    type: "polygon",
                    rings: HistoricasRing
                    };
                    // Creamos el Fill Simbol para argegar el relleno
                    const symbol = {
                        type: "simple-fill", 
                        color: [222, 159, 27, 0.5],
                        outline: { 
                            color: [255, 255, 255],
                            width: 2
                        }    
                    };
                    const popupTemplate = {
                        title: "{Name}",
                        content: [{
                            type: "fields",
                            fieldInfos: [
                                {
                                    fieldName: "Municipio",
                                    label: "Municipio",
                                },
                                {
                                    fieldName: "Provincia",
                                    label: "Provincia",
                                },
                                {
                                    fieldName: "DC",
                                    label: "DC",
                                },
                                {
                                    fieldName: "Ley",
                                    label: "Ley",
                                },
                                {
                                    fieldName: "Area",
                                    label: "Area",
                                },
                            ]
                        }]
                     }
                     const attributes = {
                        Name: items.parcela,
                        Description: `Parcela ${items}`,
                        Provincia: items.provincia,
                        Municipio: items.municipio,
                        DC: items.dc,
                        Ley: items.capa,
                        Area: items.area + " m²"
                     }
                     const graphic = {geometry, symbol, attributes, popupTemplate}
                     polygonsHistoricas.push(graphic)
                    }
                    //props.view.graphics.addMany(polygonsHistoricas);
                    setPolygonsHistoricas(polygonsHistoricas)
                }
            },[drawHistoricas, drawResultantes])//eslint-disable-line
            useGraphics(props.view, polygonsHistoricas)
            useGraphics(props.view, polygonsResultantes)
return null
}
export default Draw