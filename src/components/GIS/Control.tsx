import { Avatar, createStyles, Fab, FormControl, IconButton, Tooltip } from '@material-ui/core';
import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import GamepadIcon from '@material-ui/icons/Gamepad';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';
import CropFreeIcon from '@material-ui/icons/CropFree';
import EditIcon from '@material-ui/icons/Edit';
import LocationSearchingIcon from '@material-ui/icons/LocationSearching';
import AddLocationIcon from '@material-ui/icons/AddLocation';
import CreateIcon from '@material-ui/icons/Create';
import PublishIcon from '@material-ui/icons/Publish';
import BackspaceIcon from '@material-ui/icons/Backspace';
import { ICenter, useCenter } from './context/CenterContext';
import { IInteraction, useInteraction } from './context/InteractionContext';
import PlayForWorkIcon from '@material-ui/icons/PlayForWork';
import { Ijurisdiccion, useJurisdiccion } from './context/JurisdiccionContext';
import FetchJurisdiccion, { BackDrop } from './data/FetchJurisdiccion';
import { useEffect, useState } from 'react';
import FindReplaceIcon from '@material-ui/icons/FindReplace';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
//@ts-ignore
import proj4 from "proj4"

const useStyles = makeStyles((theme: Theme) => createStyles({
    subroot: {
        position: "absolute",
        "@media only screen and (max-width: 600px)": {
            right: "2%",
            bottom: "0",
        },
        "@media only screen and (min-width: 600px)": {
            right: "6%",
            top: "30%",
        },
        "@media only screen and (min-width: 1000px)": {
            right: "6%",
            top: "13%",
        },

    },
    root: {
        borderRadius: "50%",
        width: "105px",
        height: "105px",
        border: "1px solid transparent",
        textAlign: "center",
        borderRightColor: "transparent",
        borderLeftColor: "transparent",
        

        "& > button": {
            width: "30px",
            height: "40px",
        },
        "& > button:first-child": {
            transform: "rotate(-35deg) translateX(-0.3rem)"
        },
        "& > button:nth-child(2)": {
            transform: "translateY(-0.5rem)"
        },
        "& > button:nth-child(3)": {
            transform: "rotate(35deg)"
        },
        "& > button:nth-child(4)": {
            transform: "translateX(-1rem)"
        },
        "& > button:nth-child(5)": {
            transform: "translateX(2.7rem)"
        },
        "& > button:nth-child(6)": {
            transform: "rotate(-35deg) translateY(1.6rem) translateX(-0.8rem)"
        },
        "& > button:nth-child(7)": {
            transform: "rotate(35deg) translateY(0.1rem) translateX(-1.3rem)"
        },
        "& > button:nth-child(8)": {
            transform: "translateX(-0.8rem)"
        },
        "& > button:nth-child(9)": {
            transform: "translateX(-0.8rem)"
        },
    },
    centerer: {
        transform: "translateY(-4.4rem) translateX(1.95rem)"
    },
    zoomer: {
        "@media only screen and (max-width: 600px)": {
            maxHeight: "3rem",
            right: "2%",
            bottom: "23%"
        },
        "@media only screen and (min-width: 600px)": {
            right: "6%",
            top: "20%",
        },
        "@media only screen and (min-width: 1000px)": {
            flexFlow: "column nowrap",
            maxWidth: "3rem",
            right: "2%",
            top: "13%"
        },
        display: "flex",
        position: "absolute",
    },
    drawer:{
        "@media only screen and (max-width: 600px)": {
            right: "2%",
            top: "10%"
        },
        "@media only screen and (min-width: 600px)": {
            right: "2%",
            top: "20%",
        },
        "@media only screen and (min-width: 1000px)": {
            right: "15%",
            top: "9%",
        },
        position: "absolute",
        //@ts-ignore
        backgroundColor: drawColor=> drawColor.backgroundColor,
        borderRadius:"50%",

       "& > button > span > svg":{
        width: "15px",
        height: "15px"
       },
    },
    editer:{
        "@media only screen and (max-width: 600px)": {
            right: "2%",
            bottom: "30%"
        },
        "@media only screen and (min-width: 600px)": {
            right: "2%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            right: "2%",
            top: "33%",
        },
        position: "absolute",
        //@ts-ignore
        borderRadius:"50%",
    },
    pointer:{
        "@media only screen and (max-width: 600px)": {
            right: "2%",
            bottom: "38%"
        },
        "@media only screen and (min-width: 600px)": {
            right: "9%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            right: "2%",
            top: "43%",
        },
        position: "absolute",
        //@ts-ignore
        backgroundColor: drawMarkerColor=> drawMarkerColor.backgroundColor,
        borderRadius:"50%",
    },
    poligoner: {
        "@media only screen and (max-width: 600px)": {
           
            right: "2%",
            bottom: "45%"
        },
        "@media only screen and (min-width: 600px)": {
            right: "16%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            right: "2%",
            top: "51%",
        },
        position: "absolute",
        //@ts-ignore
        backgroundColor: drawPoligonColor=> drawPoligonColor.backgroundColor,
        borderRadius:"50%",
    }, 
    centerD:{
        "@media only screen and (max-width: 600px)": {
            right: "2%",
            bottom: "52%"
        },
        "@media only screen and (min-width: 600px)": {
            right: "23%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            right: "2%",
            top: "58%",
        },
        position: "absolute",
        //@ts-ignore
        backgroundColor: drawCenterColor=> drawCenterColor.backgroundColor,
        borderRadius:"50%",
    },
    eraser:{
        "@media only screen and (max-width: 600px)": {
            right: "2%",
            bottom: "58%"
        },
        "@media only screen and (min-width: 600px)": {
            right: "30%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            right: "2%",
            top: "65%",
        },
        position: "absolute",
        //@ts-ignore
        backgroundColor: drawCenterColor=> drawCenterColor.backgroundColor,
        borderRadius:"50%",
    }, 
    goButton: {
        "@media only screen and (max-width: 600px)": {
            left: "3%",
            bottom: "3%"
        },
        "@media only screen and (min-width: 600px)": {
            left: "3%",
            bottom: "3%",
        },
        "@media only screen and (min-width: 1000px)": {
            left: "3%",
            bottom: "5%",
        },
        position: "absolute",
        //@ts-ignore
        backgroundColor: goColored=> goColored.backgroundColor,
        borderRadius:"50%"

    },
    uploader: {
        "@media only screen and (max-width: 600px)": {
            left: "3%",
            bottom: "10%"
        },
        "@media only screen and (min-width: 600px)": {
            left: "3%",
            bottom: "15%",
        },
        "@media only screen and (min-width: 1000px)": {
            left: "3%",
            bottom: "15%",
        },
        position: "absolute",
        borderRadius:"50%",
    },
    uploaderForm: {
        "@media only screen and (max-width: 600px)": {
            left: "27%",
            top: "10%"
        },
        "@media only screen and (min-width: 600px)": {
            left: "40%",
            top: "12%",
        },
        "@media only screen and (min-width: 1000px)": {
            left: "40%",
            top: "12%",
        },
        position: "absolute",
        borderRadius:"5px",
    },
    locator: {
        "@media only screen and (max-width: 600px)": {
            right: "7%",
            top: "10%"
        },
        "@media only screen and (min-width: 600px)": {
            left: "20%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            left: "15%",
            top: "10%",
        },
        position: "absolute",
        borderRadius:"5px",
        "& button": {
            backgroundColor: "green",
        }
    },
    geocode: {
        "@media only screen and (max-width: 600px)": {
            left: "2%",
            top: "29%"
        },
        "@media only screen and (min-width: 600px)": {
            left: "28%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            left: "40%",
            top: "9%",
        },
        position: "absolute",
        backgroundColor: "hsla(100, 100%, 100%, 0.5)",
        display: "flex",
        
    },

}))
//@ts-ignore
export function ControlDirection(props) {
    const classes = useStyles()

    const mover =(where:string)=>{
        const latitude = props.view.center.latitude
        const longitude = props.view.center.longitude
        const factMov = 1e7
        const scale = props.view.scale
        const move = scale / factMov;
        let to
        switch(where){
            case "up":
                to = [longitude, latitude + move]
                props.view.goTo({center: to})
                break
            case "upleft":
                to = [longitude-move, latitude + move]
                props.view.goTo({center: to})
                break
            case "upright":
                to = [longitude+move, latitude + move]
                props.view.goTo({center: to})
                break
            case "left":
                to = [longitude-move, latitude]
                props.view.goTo({center: to})
                break
            case "right":
                to = [longitude+move, latitude]
                props.view.goTo({center: to})
                break
            case "downleft":
                to = [longitude-move, latitude - move]
                props.view.goTo({center: to})
                break
            case "down":
                to = [longitude, latitude - move]
                props.view.goTo({center: to})
                break
            case "downright":
                to = [longitude+move, latitude - move]
                props.view.goTo({center: to})
                break
            case "center":
                    to = [longitude+move, latitude - move]
                    props.view.goTo({center: [-69.61128, 18.80600]})
                    props.view.zoom = 9
                break
        }
    }
    return (
        <div className={classes.subroot}>
        <div className={classes.root}>
            <IconButton onClick={()=>(mover("upleft"))}>
            <Tooltip title="Move up-left the map view">
                <ArrowUpwardIcon />
                </Tooltip>
            </IconButton>

            <IconButton onClick={()=>(mover("up"))}>

            <Tooltip title="Move up the map View.">
                <ArrowUpwardIcon />
                </Tooltip>
            </IconButton>
            <IconButton onClick={()=>(mover("upright"))}>
            <Tooltip title="Move up-right the map view">
                <ArrowUpwardIcon />
                </Tooltip>
            </IconButton>
            <IconButton onClick={()=>(mover("left"))}>
            <Tooltip title="Move left the map view">
                <ArrowBackIcon />
                </Tooltip>
            </IconButton>
            <IconButton onClick={()=>(mover("right"))}>
            <Tooltip title="Move right the map view">
                <ArrowForwardIcon />
                </Tooltip>
            </IconButton>
            <IconButton onClick={()=>(mover("downright"))}>
            <Tooltip title="Move down-right the map view">
                <ArrowDownwardIcon />
                </Tooltip>
            </IconButton>
            <IconButton onClick={()=>(mover("downleft"))}>
            <Tooltip title="Move down-left the map view">
                <ArrowDownwardIcon />
                </Tooltip>
            </IconButton>
            <IconButton onClick={()=>(mover("down"))}>
            <Tooltip title="Move down the map view.">
                <ArrowDownwardIcon />
                </Tooltip>
            </IconButton>
        </div>
            <IconButton className={classes.centerer} onClick={()=>(mover("center"))} >
            <Tooltip title="Center The map view">
                <GamepadIcon/>
                </Tooltip>
            </IconButton>
            </div>
    );
}
//@ts-ignore
export function Zoomer(props){
    const {zoom}:ICenter = useCenter()
    const classes = useStyles()

    const zoomer =(val:string)=>{
        if(val==="up" && zoom){
            return props.view.zoom = props.view.zoom + 1
        }else  if(val==="down" && zoom){
            return props.view.zoom = props.view.zoom - 1
        }
    }
    return(
    <div className={classes.zoomer}>
        <IconButton onClick={()=>zoomer("up")}>
        <Tooltip title="Zoom in the map.">
            <ZoomInIcon />
                </Tooltip>
        </IconButton>
        <IconButton onClick={()=> zoomer("down")}>
        <Tooltip title="Zoom out the map.">
            <ZoomOutIcon />
                </Tooltip>
        </IconButton>
    </div>
    )}

export function DrawEnable(){
    const { draw }:IInteraction = useInteraction()
    const drawColor = {backgroundColor: (draw)?"hsla(120, 50%, 50%, 0.6)":"transparent"}
    const classes = useStyles(drawColor)
    const renderDraw = ()=>{
        if (draw){
        return(
            <div className={classes.drawer}>
            {/**@ts-ignore */}
            <IconButton aria-label="draw-item">
            <Tooltip title= {`The edition is ${draw?"active":"inactive"}`}>
              <CreateIcon />
                </Tooltip>
            </IconButton>
            </div>
        )}
        return (
            <></>
        )
    }
    return  renderDraw()
}


export function Point() {
    const {
            changeDrawMarker, 
            drawMarker
        }:IInteraction = useInteraction()

    const drawMarkerColor = {backgroundColor: (drawMarker)?"hsla(200, 50%, 50%, 0.3)":"transparent"}
    const classes = useStyles(drawMarkerColor)
    const handleClick = ()=>{
        changeDrawMarker && changeDrawMarker()
    }
    return (
        <div>
            {/**@ts-ignore */}
            <IconButton onClick={handleClick} className={classes.pointer} aria-label="pointer">
            <Tooltip title="Enable draw a point in the map.">
              <AddLocationIcon />
                </Tooltip>
            </IconButton>
        </div>
    );
}


export function Poligon() {

    const {
        changeDrawPoligon, 
        drawPoligon,
    }:IInteraction = useInteraction()
    const drawPoligonColor = {backgroundColor: (drawPoligon)?"hsla(200, 50%, 50%, 0.3)":"transparent"}
    const classes = useStyles(drawPoligonColor)
    const handleClick = ()=>{
        changeDrawPoligon && changeDrawPoligon()
    }
    return (
        <div>
             {/**@ts-ignore */}
            <IconButton onClick={handleClick} className={classes.poligoner} aria-label="pointer">
            <Tooltip title="Enable draw a polygon in the map.">
              <CropFreeIcon />
                </Tooltip>
            </IconButton>
        </div>
    );
}

export function Centerer(){
    
    const {
        changeDrawCenter, 
        drawCenter
    }:IInteraction = useInteraction()
    
    const drawCenterColor = {backgroundColor: (drawCenter)?"hsla(200, 50%, 50%, 0.3)":"transparent"}
    const classes = useStyles(drawCenterColor)
    return(
        <div>
        {/**@ts-ignore */}
        <IconButton onClick={changeDrawCenter} className={classes.centerD}>
        <Tooltip title="Draw A point in the centroid of the map">
            <LocationSearchingIcon />
            </Tooltip>
        </IconButton>
        </div>
    )
}

//@ts-ignore
export function Eraserer(props) {
    
    const {
        deletePoint, 
        points, 
        drawPoligon, 
        drawMarker, 
        resetPoint
    }:IInteraction = useInteraction()
    
    const drawErasererColor = {backgroundColor: (Boolean(points?.length))?"hsla(200, 50%, 50%, 0.3)":"transparent"}
    const classes = useStyles(drawErasererColor)
    const  doDeletePoint = ()=>{
        if(!points || points?.length <= 0) return
        if(!drawPoligon && !drawMarker){
            resetPoint && resetPoint()
            props.view.graphics.removeAll()
            return
        }
        const numbers = points?.length
        numbers && deletePoint && deletePoint({index: numbers - 1})
        return
    }
    return (
        <div className={classes.eraser}>
            {/**@ts-ignore */}
            <IconButton onClick={doDeletePoint} aria-label="Delete last point">
            <Tooltip title="Delete Last point or all points">
                <BackspaceIcon />
                </Tooltip>
            </IconButton>
        </div>
    );
}

export function GoButton(){
    //@ts-ignore
    const {
        changeDoSearch, 
        doSearch,
        loading,
    }:Ijurisdiccion = useJurisdiccion()

    const {
        points, 
        zoom, 
        center
    }:IInteraction = useInteraction()
    const [available, setAvailable] = useState(true)

    const goColored = {backgroundColor: (zoom && zoom >= 14 && center)
        ?"hsla(100, 50%, 50%, 0.6)":"transparent"}
    const disabled = (zoom && zoom >= 14 && center && available)?false:true
    const classes = useStyles(goColored)
  
    //@ts-ignore
    const handleClick = ()=>{
        if(!available){
            alert("Wait 60 sec for other search")

        }
        else if(available){
        let search = []
        if(!center && (points && !points.length) ) return
        // if(points && points.length <= 0){
        search.push(center)

        // }
        // else if(points && points.length > 0){
        //     for(let items of points){
        //         search.push(items)
        //     }
            //console.log(search)
        //}error?error:result &
        // changeLoading && changeLoading()
        changeDoSearch && changeDoSearch(search) 
        setAvailable(false)
        setTimeout(()=>{
            setAvailable(true)
        }, 60000
        )
       
    }
}
    
    return(
        <div className={classes.goButton}>
             {/**@ts-ignore */}

         <Fab disabled={disabled} onClick={handleClick} aria-label="Search points in points" color="primary">
         {loading?<BackDrop />:<></>}
             <Tooltip title="Start Quering the APIs">
            <PlayForWorkIcon />
             </Tooltip>
         </Fab>
         {doSearch && doSearch.length > 0 && <FetchJurisdiccion doSearch={doSearch} renderSucces={()=>(<></>)}/>}
        </div>
    );
}


export function UploadFile(){
    const [showForm, setShowForm] = useState<boolean>(false)
    const [readedFile, setFileReaded] = useState()
    const classes = useStyles()
    //@ts-ignore
    const handleFileChosen =(file)=>{
        let reader = new FileReader()
        //@ts-ignore
        reader.onload = ()=>setFileReaded(reader.result)
        reader.readAsArrayBuffer(file);
        //@ts-ignore
        setFileReaded(reader)
        
        }
    useEffect(()=>{
        if(!readedFile)return
            //@ts-ignore
            console.log(readedFile)
        
    },[readedFile])
    const fileForm = ()=>setShowForm(!showForm)
    return(
        <>
        <div className={classes.uploader}>
        <IconButton onClick={fileForm} aria-label="Search points in points">
             <Tooltip title="Upload a File">
            <PublishIcon />
             </Tooltip>
         </IconButton>
        </div>
        {showForm?(
    <div className={classes.uploaderForm}>
    <input
      type='file'
      id='file'
      className='input-file'
      accept='.csv'
      onChange={e => handleFileChosen(e && e.target && e.target.files && e.target.files[0])}
    />
  </div>):<></>}
        </>
    )
} 

//@ts-ignore
export function EditButton({view}){
    const [show, setShow] = useState(false)
    const classes= useStyles(show)

    const renderShow = ()=>{
        setShow(!show)
        }

return(
<>

    <div className={classes.editer}>
<Tooltip title="Show the edition tools.">
        <Fab onClick={renderShow} color={`${show?"secondary":"primary"}`}>
            <EditIcon />
        </Fab>
</Tooltip>
    </div>
    <>
        {show?
             <>
             <Point/>
             <Poligon />
             <Eraserer view={view}/>
             <Centerer />
             </>
             :<></>
         }
        </>
        </>
)
}

//@ts-ignore
export function GetLocation({view}){
    const classes = useStyles()
    const handleClick = ()=>{
        if("geolocation" in navigator){
            navigator.geolocation.getCurrentPosition(pos=>{
                view.goTo({center: [pos.coords.longitude, pos.coords.latitude]})
                view.zoom = 16
        }, (err)=>{alert(err.message)}, { enableHighAccuracy: false, timeout: 5000},
        )}
        else{
            alert("Your browser does not have Geolocation active sorry.")
        }
    }
    return(
        <div className={classes.locator}>
            <Fab onClick={handleClick} color="primary">
            <LocationSearchingIcon />
            </Fab>
        </div>
    )
}

//@ts-ignore
export function GeoCoder({view}){
    const [value, setValue] = useState("geographic")
    const [openForm, setOpenForm] = useState(false)
    const [latlon, setLatLon] = useState({lat:"0", lon:"-0"})
    const [northEast, setNorthEast] = useState({north:"0", east:"0"})
    const [direction, setDirection] = useState("distrito nacional, santo domingo")
    const {center}:IInteraction = useInteraction()
    const classes = useStyles()

    //@ts-ignore
    const handleClick = () =>{
        let utm = '+proj=utm +ellps=GRS80 +datum=nad83 +units=m +no_defs +zone=19N';
        let latlong = '+proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees +no_defs';
      
            if(value === "geographic"){
                view && view.goTo({center: [Number(latlon.lon), Number(latlon.lat)]});
                view.zoom = 14;

            }
            else if(value === "utm"){
                if(center) {
                let valor = proj4( utm, latlong, [Number(northEast.east), Number(northEast.north)])
                view && view.goTo({center: [valor[0], valor[1]]});
                view.zoom = 14
                console.log(valor)
                console.log(northEast)
                }
            }
            else if(value === "geocoding"){
                if (view){
                    const overpassURL = new URL(`https://nominatim.openstreetmap.org/search`)
                    const params = {
                        q: direction + " republica dominicana",
                        // city: "haina",
                        // county: "san cristobal",
                        // country: "dominican republic",
                        format: "json",
                    }
                    overpassURL.search = new URLSearchParams(params).toString();
                    //@ts-ignore
                    fetch(overpassURL).then(
                        body=>body.json()
                    ).then(
                        (result)=>{
                            if (!result.length){
                                return alert("Search didnt find place.")
                            }
                            else{
                                let elected = result[0]
                                const {lat:lat1, lon:lon1} =  elected
                                view && view.goTo({center: [Number(lon1), Number(lat1)]});
                                view.zoom = 15
                                console.log("Found place, going to the first coincidence.")
                                console.log(elected)
                            }
                        }
                    ).catch(err=>console.log(err))
                }
            }
            
    }
    const handleChange = (event: React.ChangeEvent<HTMLInputElement>)=>{
        setValue((event.target as HTMLInputElement).value);
        console.log(value)
    }
    const handleGeographic = (event: React.ChangeEvent<HTMLInputElement>, where: string)=>{
        let change = (event.target as HTMLInputElement).value
        
        if (change !== change){//eslint-disable-line
            alert("Only numbers can be assign to these camps")
            change = "0"
        }
        if(where === "lon"){
            change = change  //eslint-disable-line
        }
        let obj = {}
        //@ts-ignore
        obj[where] = change
        setLatLon({...latlon, ...obj})
    }
    const handleUTM = (event: React.ChangeEvent<HTMLInputElement>, where: string)=>{
        let change = Number((event.target as HTMLInputElement).value)
        
        if (change !== change){//eslint-disable-line
            alert("Only numbers can be assign to these camps")
            return
        }
        let obj = {}
        //@ts-ignore
        obj[where] = change
        setNorthEast({...northEast, ...obj})
    }
    const handleDirection = (event: React.ChangeEvent<HTMLInputElement>)=>{
        let change = (event.target as HTMLInputElement).value
        setDirection(change)
    } 
    useEffect(()=>{
        if(!openForm) return
        let utm = '+proj=utm +ellps=GRS80 +datum=nad83 +units=m +no_defs +zone=19N';
        let latlong = '+proj=longlat +ellps=WGS84 +datum=WGS84 +units=degrees +no_defs';
        let obj = {lat: "", lon: ""}
        let objUTM = {north: "", east: ""}
        if(center){
            obj.lat = center[0]
            obj.lon = center[1]
            //@ts-ignore
            let item = proj4(latlong, utm, [Number(center[1]), Number(center[0])])
            objUTM.north = item[1]
            objUTM.east = item[0]
            setLatLon({...obj})
            setNorthEast({...objUTM})
        }

    },[center]) //eslint-disable-line
    const renderForm = ()=>{
        return(
            <>
            <div>
        <FormControl component="fieldset">
        <FormControl>
        <InputLabel id="demo-simple-select-label">Select</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={value}
          //@ts-ignore
          onChange={handleChange}
        >
          <MenuItem value="geographic">Geographic</MenuItem>
          <MenuItem value="utm">UTM</MenuItem>
          <MenuItem value="geocoding">Geocoding</MenuItem>
        </Select>
      </FormControl>
        </FormControl>
        </div>
        <div>
        <form>
            {value==="geographic"?
                <>
                {/* @ts-ignore*/}
                    <TextField required id="standard-required" label="Latitud" value={latlon.lat} onChange={(e)=>{handleGeographic(e, "lat")}}/>
                    {/* @ts-ignore*/}
                    <TextField required id="standard-required" label="Longitud" value={latlon.lon} onChange={(e)=>{handleGeographic(e, "lon")}}/>
                </>
            :value === "utm"
            ?   <>
                    {/* @ts-ignore*/}
                    <TextField required id="standard-required" label="Notrh" value={northEast.north} onChange={(e)=>{handleUTM(e, "north")}}/>
                    {/* @ts-ignore*/}
                    <TextField required id="standard-required" label="East" value={northEast.east} onChange={(e)=>{handleUTM(e, "east")}}/>
                </>
            :value === "geocoding"
            ?   <>
                     <TextField style={{width:"25vw"}} required id="standard-required" label="Direction" value={direction} onChange={handleDirection}/>
                </>
            : <></>}
            <IconButton onClick={handleClick}>
                <FindReplaceIcon /> 
            </IconButton>
        </form>
        </div>
        </>
        )
    }
    return(
        <div className={classes.geocode}>
            <IconButton onClick={()=>setOpenForm(!openForm)}>
            <Avatar>Geo</Avatar>
            </IconButton>
            {openForm && renderForm()}
        </div>
    )
}
