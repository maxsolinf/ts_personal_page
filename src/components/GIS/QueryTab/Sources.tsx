import GpsFixedIcon from '@material-ui/icons/GpsFixed';
import SearchIcon from "@material-ui/icons/Search"
import { makeStyles, Typography } from "@material-ui/core"
import { createStyles } from "@material-ui/core"
import { Theme, Grid } from "@material-ui/core"
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import { IInteraction, useInteraction } from "../context/InteractionContext";
import { Avatar } from "@material-ui/core";
import { Tooltip } from "@material-ui/core";
import { Badge } from "@material-ui/core";
import TreeItem from '@material-ui/lab/TreeItem';
import TreeView from '@material-ui/lab/TreeView';

import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Ijurisdiccion, useJurisdiccion } from "../context/JurisdiccionContext";
import { ChangeEvent } from "react";



const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        backgroundColor: "hsla(220, 50%, 50%, 0.8)",
        borderRadius: "20px",
        textAlign:"center",
        marginTop: "7px",
        overflowX: "auto",
        color: "hsl(255, 50%,100%)",
        
    },
    accordeon:{
        backgroundColor: "hsla(240, 50%, 50%, 0.8)",
        width: "100%",
        color: "white"
    },
    accordeonIcon:{
        height: theme.spacing(2),
        width: theme.spacing(2),
        color: "white",
    },
    pointAvatar: {
        width: theme.spacing(3),
        height: theme.spacing(3),
        color: "hsla(240, 50%, 50%, 0.8)",
        backgroundColor: "hsla(200, 100%, 100%, 0.5)"
    },
}))


export function Sources(){
    
    return(
        <Grid container spacing={1}>
        {CurrentLocation()}
        {CollectingInfo()}
        {SelectSource()}
        </Grid>
)
}
function CurrentLocation(){
    const classes = useStyles()
    // const {zoom, center}:ICenter = useCenter()
    const {zoom, center}:IInteraction = useInteraction()
    return(
        <Grid container spacing={1} className={classes.root} alignContent="center" justifyContent="center">
            <Grid item xs={12}>
            <Typography variant="h5" color="initial">Current Location</Typography>
              
            </Grid>
            <Grid item >
                <SearchIcon />
                <Typography variant="caption" color="initial">
                    Zoom: {zoom}
                </Typography> 
            </Grid>
            <Grid item  >
                <GpsFixedIcon />
                <Typography variant="caption" color="initial">
                    center: Lat:{center && center[0]} Long:{center && center[1]}
                </Typography>
            </Grid>
        </Grid> 
    )
}

function CollectingInfo() {
    const classes = useStyles()
    const {points, deletePoint }:IInteraction = useInteraction()
    const pointAdder = ()=>{
        let item
        if(points && points.length && deletePoint){
            //@ts-ignore
            item = points.map((value,idx)=>(
            <Grid item  xs={1} onClick={()=>deletePoint({index:idx})}>
                <Tooltip title={`Point ${idx}: ${value}`}>
                <Avatar className={classes.pointAvatar}>
                {idx+1}
                </Avatar>
                </Tooltip>
            </Grid>))
            return item
        }
        return
    }
    return(
        <Grid container className={classes.root} wrap="wrap">
          <Accordion className={classes.accordeon} >
              <AccordionSummary expandIcon={<ExpandMoreIcon className={classes.accordeonIcon}/>} aria-controls="vertices-content" id="vertices-header">
              <Badge badgeContent={points && points.length} color="primary">
                  <Typography variant="body2" color="inherit" >COLLECTED VERTICES.</Typography> 
                  </Badge>
              </AccordionSummary>
              <AccordionDetails>
                  <Grid container className={classes.root}>
                  {pointAdder()}
                  </Grid>
              </AccordionDetails>
          </Accordion>
        </Grid>
    )
}
function SelectSource() {
    const {getResultantes, getHistoricas, changeGetHistoricas, changeGetResultantes}:Ijurisdiccion = useJurisdiccion()
    const classes = useStyles()
    //@ts-ignore
    const handlehistoricas = (e:ChangeEvent<HTMLInputElement>)=>{
        changeGetHistoricas && changeGetHistoricas()
    }
    const handleResultantes = (e:ChangeEvent<HTMLInputElement>)=>{
        changeGetResultantes && changeGetResultantes()
    }
    return(
        <Grid container wrap="wrap" className={classes.root}>
          <Accordion className={classes.accordeon} >
              <AccordionSummary expandIcon={<ExpandMoreIcon className={classes.accordeonIcon}/>} aria-controls="vertices-content" id="vertices-header">
                Source Selection.
              </AccordionSummary>
              <AccordionDetails>
                  <Grid container spacing={1} className={classes.root}>
                  <Grid item sm={12} className={classes.root}>
                    <TreeView defaultCollapseIcon={<ExpandMoreIcon/>} defaultExpandIcon={<ChevronRightIcon />} >
                        <TreeItem nodeId="1" label="JI Parcelas">
                            <TreeItem nodeId="2" label="Parcelas">
                                <FormControl>
                                    <FormGroup>
                                        <FormControlLabel control={<Checkbox checked={getHistoricas && getHistoricas} onChange={handlehistoricas} name="gilad" />} label="Historicas"/>
                                        <FormControlLabel control={<Checkbox checked={getResultantes && getResultantes} onChange={handleResultantes} name="gilad" />} label="Resultantes"/>
                                    </FormGroup>
                                </FormControl>
                            </TreeItem>
                        </TreeItem>
                    </TreeView>
                  </Grid>
                  
                  </Grid>
              </AccordionDetails>
          </Accordion>
        </Grid>
    )
}