import { makeStyles, Tab, Tabs } from '@material-ui/core';
import { createStyles } from '@material-ui/core';
import { AppBar } from '@material-ui/core';
import { Box } from '@material-ui/core';
import { Tooltip } from '@material-ui/core';
import { Theme } from '@material-ui/core';
import AssignmentIcon from '@material-ui/icons/Assignment';
import { useState } from 'react';
import { Sources } from './Sources';
import Results from '../ResultTab/Results';
import Fab from '@material-ui/core/Fab';



interface TabPanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
  }

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        "@media only screen and (max-width: 600px)": {
            left: "2%",
            top: "10%",
        },
        "@media only screen and (min-width: 600px)": {
            left: "6%",
            top: "10%",
        },
        "@media only screen and (min-width: 1000px)": {
            left: "1%",
            top: "10%",
        },
        position: "absolute",
        //@ts-ignore
        //backgroundColor: selectionMenuColor=> selectionMenuColor.backgroundColor,
    },
    selectionBox: {
        "@media only screen and (max-width: 600px)": {
            left: "2%",
            top: "18%",
            width: "80vw",
        },
        "@media only screen and (min-width: 600px)": {
            left: "6%",
            top: "16%",
            width: "57%",
        },
        "@media only screen and (min-width: 1000px)": {
            left: "1%",
            top: "16%",
            width: "38%",
        },
        "&":{
            height: "80vh",
            position: "absolute",
            backgroundColor:"hsla(200, 50%, 50%, 0.4)",
            borderRadius: "20px",
            overflowY:"auto"
        },

    }

}))
export function SelectionMenu(){
    const [drawer, setDrawer] = useState<boolean>(false)
    const [value, setValue] = useState<number>(0);
    const handleClick = ()=>{
        setDrawer(!drawer)
    }

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
      };

    const selectionMenuColor = {backgroundColor: (drawer)?"hsla(200, 50%, 50%, 0.3)":"transparent"}
    const classes = useStyles(selectionMenuColor)
    const selectBox = ()=>{
        return(
            <div className={classes.selectionBox}>
                <AppBar position="static">
                    <Tabs value={value} onChange={handleChange}>
                        <Tab label="Source" id="tab-sources">
                        </Tab>
                        <Tab label="Results" id="tab-results">
                        </Tab>
                        <Tab label="Config" id="tab-config" >
                        </Tab>
                    </Tabs>
                </AppBar>
                    <TabPanel value={value} index={0}>
                    <Sources />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                    <Results />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                    Item Three
                    </TabPanel>


            </div>
        )
    }
    return(
        <div>
            <Fab onClick={handleClick} color={`${drawer?"secondary":"primary"}`} className={classes.root} aria-label="Open Selection Menu">
            <Tooltip title="Open Control panel with the distinct options and results">
                <AssignmentIcon />
                </Tooltip>
            </Fab>
            {drawer && selectBox()}
        </div>
    )

}

function TabPanel(props: TabPanelProps){
    const { children, value, index, ...other } = props;
    return (
        <div
            role="tabpanel"
            hidden={value!==index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
        </div>
    )
}
// function a11yProps(index: any) {
//     return {
//       id: `simple-tab-${index}`,
//       'aria-controls': `simple-tabpanel-${index}`,
//     };
//   }

