import Draw from "./Graphics"
import MapC from "./MapC";
import { IInteraction, useInteraction } from "./context/InteractionContext"

const GisC = ()=>{
    const {addPoint, draw}:IInteraction = useInteraction()
  //@ts-ignore
    const clickedMap = (e)=>{
        addPoint && draw && addPoint(e.mapPoint)
    }
    
    return(
      <>
        {/**@ts-ignore */}
      <MapC clickedMap={clickedMap}>
          <Draw />
      </MapC>
       </>
    )

}
export default GisC