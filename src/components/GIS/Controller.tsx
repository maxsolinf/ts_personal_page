import {
    BsArrowDown,
    BsArrowUp,
    BsArrowRight,
    BsArrowLeft,
    BsArrowDownLeft,
    BsArrowDownRight,
    BsArrowUpLeft,
    BsArrowUpRight,
} from 'react-icons/bs';
import {
    AiOutlineZoomIn,
    AiOutlineZoomOut
} from 'react-icons/ai';
import { ControllerS } from '../../styles/gis/LeftContent';
import { IInteraction, useInteraction } from './context/InteractionContext';
//@ts-ignore
export default function ControllerC(props){
    const {zoom}:IInteraction = useInteraction()

    const zoomer=(val:string)=>{
        if(val==="up" && zoom){
            return props.view.zoom = props.view.zoom + 1
        }else  if(val==="down" && zoom){
            return props.view.zoom = props.view.zoom - 1
        }
    }
    const mover =(where:string)=>{
        const latitude = props.view.center.latitude
        const longitude = props.view.center.longitude
        const factMov = 1e7
        const scale = props.view.scale
        const move = scale / factMov;
        let to
        switch(where){
            case "up":
                to = [longitude, latitude + move]
                props.view.goTo({center: to})
                break
            case "upleft":
                to = [longitude-move, latitude + move]
                props.view.goTo({center: to})
                break
            case "upright":
                to = [longitude+move, latitude + move]
                props.view.goTo({center: to})
                break
            case "left":
                to = [longitude-move, latitude]
                props.view.goTo({center: to})
                break
            case "right":
                to = [longitude+move, latitude]
                props.view.goTo({center: to})
                break
            case "downleft":
                to = [longitude-move, latitude - move]
                props.view.goTo({center: to})
                break
            case "down":
                to = [longitude, latitude - move]
                props.view.goTo({center: to})
                break
            case "downright":
                to = [longitude+move, latitude - move]
                props.view.goTo({center: to})
                break
        }
    }
    return(
        <>
        <ControllerS>
            {/*@ts-ignore*/}
            <BsArrowUpLeft className="upleft" onClick={()=>(mover("upleft"))} />
            <BsArrowUp className="up" onClick={()=>mover("up")}/>
            <BsArrowUpRight className="upright" onClick={()=>mover("upright")}/>
            <BsArrowLeft className="left" onClick={()=>mover("left")}/>
            <BsArrowRight className="right" onClick={()=>mover("right")}/>
            <BsArrowDown className="down" onClick={()=>mover("down")}/>
            <BsArrowDownLeft className="downleft" onClick={()=>mover("downleft")}/>
            <BsArrowDownRight className="downright" onClick={()=>mover("downright")}/>
            <AiOutlineZoomIn className="zoomin" onClick={()=>zoomer("up")}/>
            <AiOutlineZoomOut className="zoomout" onClick={()=>zoomer("down")}/>
        </ControllerS>
        </>
    )
}