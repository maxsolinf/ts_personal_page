import useFetchJurisdiccion from "./useFetchJurisdiccion";
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Backdrop from '@material-ui/core/Backdrop';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    backdrop: {
      zIndex: theme.zIndex.drawer + 1,
      color: '#fff',
    },
  }),
);

function FetchJurisdiccion({
    //@ts-ignore 
    doSearch,
    loadingFallback=<h1>Loading</h1>,
    renderError=(<>error</>),
    //@ts-ignore 
    renderSucces
}) {
    console.log("Calling Fetch")
    //@ts-ignore
    const [result, loading, error]= useFetchJurisdiccion(doSearch)

    return (
        loading?<BackDrop/>:(error)?<p>Error</p>:renderSucces(result)
    );
}
//@ts-ignore
export function BackDrop(){
    const classes = useStyles();
    return(
        <div>
        <Backdrop className={classes.backdrop} open={true}>
            <CircularProgress color="inherit" />
        </Backdrop>
        </div>
    )
}

export default FetchJurisdiccion;