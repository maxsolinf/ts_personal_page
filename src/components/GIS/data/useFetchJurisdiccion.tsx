import { useState } from "react";
import { useCallback } from "react";
import { useEffect } from "react";
import { Ijurisdiccion, useJurisdiccion} from "../context/JurisdiccionContext";

type puntos = {
    x?: string
    y?:string
}
interface IresponseJI{
        numeroExpediente: string,
        parcela: string,
        dc: string,
        operacion: string,
        parcelaTemporal: string,
        area: Number,
        fechaInscripcion: Date,
        posicional: string,
        provincia: string,
        municipio: string,
        polygons: null,
        puntos?: puntos[] 
        capa: string
      
}
interface IrootResponse {
    coordinates:string,
    polygons: IresponseJI[]
}

//@ts-ignore
function useFetchJurisdiccion(doSearch) {
    //console.log("Llamando usefetch")
    const {setSearchResult, searchResult, changeDoSearch, loading, changeLoading}:Ijurisdiccion = useJurisdiccion()
    const [error, setError] = useState()
    // const [loading, setLoading] = useState(false)
    

    const postProccess = useCallback(async (
        value:IrootResponse, 
        historicas:Map<string, string|number|object>,
        resultantes:Map<string, string|number|object>
        )=>{        
        const {polygons} = value
        if(polygons.length<= 0)return 
        for(let item of polygons){
            if(item.capa === "Parcelas Ley 108-05"){
                resultantes.set(`${item.numeroExpediente}`, item)
            }
            else if(item.capa === "Parcelas Ley 1542"){
                historicas.set(`${item.parcela}`, item)
                
            }
    
        }
        return
    },[])
    
    useEffect(()=>{ 
        if(doSearch && !doSearch.length) return      
        
        changeLoading && changeLoading(true)
        setSearchResult && setSearchResult(undefined)
        let historicas = new Map()
        let resultantes = new Map()
        let todoCatastro = new Map()
        let counter = 0
        for (let item of doSearch){
            const params = new URLSearchParams();
            params.append("value", `${item[0]},${item[1]}`)
            counter++
            if(counter === doSearch.length){
                //@ts-ignore
                consulta(params, historicas, resultantes, todoCatastro)}
            else(
                consulta(params, historicas, resultantes, todoCatastro, changeLoading)
            )

        }
        return ()=>{}
        

    },[doSearch])//eslint-disable-line
                
    useEffect(()=>{
        if(!searchResult)return
        changeDoSearch && changeDoSearch([])
        return ()=>{}
    },[searchResult])//eslint-disable-line
  
    //@ts-ignore
    async function consulta(params, historicas, resultantes, todoCatastro, setLoading){
        fetch("https://servicios.ri.gob.do/ConsultaGeografica/ConsultarContorno", {
            method: "post",
            body: params,})
        .then(body=>body.json())
        .then(
            body=>postProccess(body, historicas, resultantes)
        )
        .then(()=>{
            todoCatastro.set("historicas", historicas)
            todoCatastro.set("resultantes", resultantes)
            setSearchResult && setSearchResult(todoCatastro)
            
                changeLoading && changeLoading(false)
                console.log("ending fetch")
         
        }
        ).catch(error=>{
            console.log(error)
            setError(error)
        })
        return
    }

    return [searchResult, loading, error]
}



export default useFetchJurisdiccion;