import { Container, createStyles, makeStyles } from "@material-ui/core";
import { useMap, useWatch, useEvent} from "esri-loader-hooks"
import { useInteraction } from "./context/InteractionContext";
import {  DrawEnable, EditButton, GoButton, UploadFile, Zoomer, GetLocation, GeoCoder} from "./Control";
import { ControlDirection } from "./Control";
import Draw from "./Graphics";
import { SelectionMenu } from "./QueryTab/QuerySelectors";

const useStyles = makeStyles((theme) => createStyles({
    root:{
        "@media only screen and (max-width: 600px)": {
            height: "calc(100vh - 56px)",
            "& div.esri-ui-inner-container:nth-child(2)":{
                padding: "10px 10px",
                top: "19%",
                left: "2%",
            }
        },

        "@media only screen and (min-width: 600px)": {
        height: "calc(100vh - 65px)",
        "& div.esri-ui-inner-container:nth-child(2)":{
            padding: "10px 10px",
            top: "19%",
            left: "2%",
        }
    },
        width: "100vw",
        margin: "0 0",
        paddingLeft:"0",
        "&  .esri-attribution":{
            display: "none"
        },
        "& div.esri-zoom" :{
            display:"none"
        },
        "& div.esri-ui-inner-container:nth-child(2)":{
            border: "1px solid white",
            backgroundColor: "hsla(220, 50%, 50%, 0.5)",
            borderRadius: "15px",
            minWidth: "1px",
            position: "absolute",
        },
        "& .esri-popup__main-container":{
            display: "flex",
            flexFlow: "column nowrap",
            alignContent: "center",
            textAlign: "center",
            justifyContent: "center",

        },
        "& .esri-popup__action": {
            border: "2px solid black",
            borderRadius: "8px",
            maxWidth: "50%",
            margin: "auto auto",
            backgroundColor:"hsla(170, 50%, 50%, 0.7)",
            color: "white",

        }

       

    },
    control: {
        position: "absolute",
        bottom: "0",
        rigth: "50%",
    }
}))

export default function MapC({clickedMap = f=>f}){
   const {centerChange,center, zoom, changeZoom, basemap} = useInteraction()
    const classes = useStyles()
   const map ={
       basemap,
    }
    
    const options = {
        view: {
            scale: 50000,
            center,
            zoom,
        }
    }

    const [mapRef, mapView] = useMap(map, options)

    const onCenterChange = ()=>{
        const {latitude, longitude} = mapView.center
        centerChange(latitude, longitude)
    }
    const onChangeZoom = (e)=>{
        changeZoom(Number(e).toFixed(0))
    }
        
    useWatch(mapView, 'center', onCenterChange)
    useWatch(mapView, 'zoom', onChangeZoom)
    useEvent(mapView, "click", clickedMap)  
    
    // {mapView && <ControllerC view={mapView} />}
    return (
        <Container maxWidth="xl" className={classes.root} ref={mapRef} >
            {mapView &&<Draw view={mapView} />}
            {mapView && <ControlDirection view={mapView} />}
            {mapView && <Zoomer view={mapView} />}
            {mapView && <DrawEnable />}
            {mapView && <UploadFile />}
            {mapView && <GoButton />}
            {mapView && <EditButton view={mapView}/>}
            {mapView && <GetLocation view={mapView}/>}
            {mapView && <GeoCoder view={mapView}/>}
            {mapView && <SelectionMenu />}
        </Container>
    )
}