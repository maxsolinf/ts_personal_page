import { useState, useCallback } from "react";
import { useContext } from "react";
import { createContext } from "react";
import { ReactElement } from "react-markdown";

interface IJurisdiccionProvider{
    children: React.PropsWithChildren<ReactElement>
}
export interface Ijurisdiccion{ 
    getResultantes?: boolean,
    getHistoricas?: boolean,
    loading?:Boolean,
    listHistoricas?: Object[],
    listResultantes?: Object[],
    selectedListResultantes?: string[], 
    selectedListHistoricas?: string[], 
    searchResult?: Map<string, Map<string, Object>>,
    doSearch?: string[][],
    changeGetResultantes?: Function,
    changeGetHistoricas?: Function,
    changeDoSearch?: Function,
    setSearchResult?: Function, 
    setListHistoricas?: Function,
    setListResultantes?: Function,
    setSelectedListResultantes?: Function,
    setSelectedListHistoricas?: Function,
    setLoading?:Function,
    changeLoading?: Function,
}

//@ts-ignore
const JurisdiccionContext = createContext({});

export const useJurisdiccion = ()=>useContext(JurisdiccionContext)

export type listPositions = [
    [string, string]
]
export function JurisdiccionProvider({children}:IJurisdiccionProvider){
    const [getResultantes, setGetResultantes] = useState<boolean>(false);
    const [getHistoricas, setGetHistoricas] = useState<boolean>(false);
    const [searchResult, setSearchResult] = useState();
    const [doSearch, setDosearch] = useState<string[][]>([]);
    const [listHistoricas, setListHistoricas] = useState([])
    const [listResultantes, setListResultantes] = useState([])
    const [selectedListResultantes, setSelectedListResultantes] = useState([])
    const [selectedListHistoricas, setSelectedListHistoricas] = useState([])
    const [loading, setLoading] = useState(false)

    const  changeGetResultantes = useCallback (()=>{
        setGetResultantes(!getResultantes)
    }, [getResultantes])

    const changeGetHistoricas = useCallback(()=>{
        setGetHistoricas(!getHistoricas)
    }, [getHistoricas])

    const changeDoSearch = useCallback((listPosition:listPositions)=>{
        setDosearch([])
        setDosearch([...listPosition])
    },[])

    const changeLoading = useCallback((valor:Boolean)=>{
        //@ts-ignore
        setLoading(valor)
    },[setLoading])

    return(
        <JurisdiccionContext.Provider value={{
            getResultantes, 
            getHistoricas, 
            searchResult, 
            doSearch, 
            loading,
            listHistoricas,
            listResultantes,
            selectedListResultantes, 
            changeGetHistoricas, 
            selectedListHistoricas, 
            changeGetResultantes, 
            changeDoSearch, 
            setSearchResult,
            setListHistoricas,
            setListResultantes,
            setSelectedListResultantes,
            setSelectedListHistoricas,
            setLoading,
            changeLoading
            }}>
            {children}
        </JurisdiccionContext.Provider>
    )
} 