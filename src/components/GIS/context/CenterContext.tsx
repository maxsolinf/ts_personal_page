import { createContext, useContext, useState } from "react";
import { ReactElement } from "react-markdown";

const CenterContext = createContext({})

export const useCenter = ()=>useContext(CenterContext)
interface ICenterProvider {
    children:React.PropsWithChildren<ReactElement>
}
export interface ICenter{
    center?: string[],
    zoom?: number,
    basemap?: string,
    changeZoom?: Function
    changeCenter?:Function
    setBasemap?:Function
}

export const CenterProvider = ({children}:ICenterProvider)=>{
    const [center, setCenter] = useState(["-69.61128", "18.80600"])
    const [zoom, setZoom] = useState(8)
    const [basemap, setBasemap] = useState("streets")
    const centerChange = (lat:number, lon:number)=>{
        setCenter([lat.toString(), lon.toString()])
    } 
    const changeZoom = (num:number)=>{
        setZoom(num)
    }
    return(
    <CenterContext.Provider value={{center, zoom, basemap, centerChange, changeZoom, setBasemap}}>
        {children}
    </CenterContext.Provider>    
    )
}