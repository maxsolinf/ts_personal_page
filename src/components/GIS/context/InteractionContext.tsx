import React, { createContext, ReactElement, useContext, useState } from "react";

const InteractionContext = createContext({})

interface IInteractionProvider{
    children: React.PropsWithChildren<ReactElement>
}
export type point = {
    latitude?: number
    longitude?: number
    index?: number 
}
export interface IInteraction{
    points?: number[],
    draw?: boolean,
    drawMarker?: boolean,
    drawPoligon?: boolean,
    drawCenter?: boolean,
    drawHistoricas?: boolean,
    drawResultantes?: boolean,
    addPoint?: Function,
    deletePoint?: Function,
    resetPoint?: Function,
    center?: string[],
    zoom?: number,
    basemap?: string,
    changeZoom?: Function
    changeCenter?:Function
    setBasemap?:Function
    changeDraw?: Function,
    changeDrawMarker?: Function,
    changeDrawPoligon?: Function,
    changeDrawCenter?: Function,
    setDrawHistoricas?: Function,
    setDrawResultantes?: Function,
}
export const useInteraction = ()=>useContext(InteractionContext)

export const InteractionProvider = ({children}:IInteractionProvider)=>{
    const [center, setCenter] = useState(["-69.61128", "18.80600"])
    const [basemap, setBasemap] = useState("streets")
    const [zoom, setZoom] = useState(8)
    const [points, setPoints] = useState<number[]>([])
    const [draw, setDraw] = useState<boolean>(false)
    const [drawMarker, setDrawMarker] = useState<boolean>(false)
    const [drawCenter, setDrawCenter] = useState<boolean>(false)
    const [drawPoligon, setDrawPoligon] = useState<boolean>(false)
    const [drawHistoricas, setDrawHistoricas] = useState()
    const [drawResultantes, setDrawResultantes] = useState()

    // old Center context
    const centerChange = (lat:number, lon:number)=>{
        setCenter([lat.toString(), lon.toString()])
    } 
    const changeZoom = (num:number)=>{
        setZoom(num)
    }
    // end
    const addPoint = (obj:point)=>{
        const {latitude, longitude} = obj
        // console.log(typeof latitude)
        //@ts-ignore
        setPoints([...points, [latitude, longitude]])
        //console.log("Added Points")
    }
    const deletePoint = (obj:point)=>{
        const {index} = obj
        let newArr = points.filter((val, idx)=>idx!==index)
        setPoints(newArr)
        console.log("deleting point", index)
        console.log(newArr)
    }
    const resetPoint= ()=>{
        setPoints([])
    }
    const changeDraw = ()=>{
        setDraw(!draw)
    }
    const changeDrawMarker = ()=>{
        if(!draw){
        setDraw(!draw)
        setDrawMarker(!drawMarker)
            return
    }
        if(!drawMarker&&drawPoligon){
            setDrawPoligon(!drawPoligon)
            setDrawMarker(!drawMarker)
        }
        else{
            setDraw(!draw)
            setDrawMarker(!drawMarker)
        }
    }
    const changeDrawPoligon = ()=>{
        if(!draw){
        setDraw(!draw)
        setDrawPoligon(!drawPoligon)
        return
    }
        if(!drawPoligon && drawMarker){
            setDrawPoligon(!drawPoligon)
        setDrawMarker(!drawMarker)
        }
        else{
            setDraw(!draw)
            setDrawPoligon(!drawPoligon)
        }
    }
    const changeDrawCenter = ()=>{
        setDrawCenter(!drawCenter)
    }

    return(
        <InteractionContext.Provider value={{
            points, 
            draw, 
            drawMarker, 
            drawPoligon, 
            drawCenter, 
            drawHistoricas,
            drawResultantes,
            center, 
            zoom, 
            basemap, 
            centerChange, 
            changeZoom, 
            setBasemap,
            addPoint, 
            deletePoint, 
            resetPoint,
            changeDraw, 
            changeDrawMarker, 
            changeDrawPoligon,
            changeDrawCenter,
            setDrawHistoricas,
            setDrawResultantes,
            }}>
        {children}
        </InteractionContext.Provider>
    )
}