import { useState } from "react"
import { useCenter , ICenter} from "./context/CenterContext"
import { useInteraction , IInteraction} from "./context/InteractionContext"
import { LeftbarS, PointStorage } from "../../styles/gis/LeftContent"

const Leftbar = ()=>{
    const {center, zoom, setBasemap}:ICenter = useCenter()
    const [leftControl, setLeftControl] = useState({control: false})
    const {
        points, 
        draw, 
        deletePoint, 
        drawMarker, 
        drawPoligon,
        changeDraw, 
        changeDrawMarker, 
        changeDrawPoligon, 
        changeDrawCenter,
    }:IInteraction = useInteraction()
    
    const buttonStyle = (item:boolean)=>({
        background:item?"green":"red"
    })
    //@ts-ignore
    const clicked = (item)=>{
        //@ts-ignore
        setLeftControl({[item]: !leftControl[item]})
    }
    const changedMap = (e:string)=>{
        setBasemap && setBasemap(e)
    }
    const renderPoints=()=>{
        let devuelta:JSX.Element[] = []
        if(points){
            devuelta = points.map((value, idx:number)=>{
                //@ts-ignore
                return <p className="point" key={value[0]+value[1]+idx} onClick={()=>deletePoint({index:idx})}>{`P${idx + 1}`}</p>
            })
        }
        return devuelta
    }

    return(
        <LeftbarS>
        
            <button onClick={()=>clicked("control")}>O</button>
            {leftControl.control ?
            <div className="control position">
                <h2>Position Control</h2>
                    {center && 
                    <><p>Lat: {center && center[0]} &nbsp; Long: {center && center[1]}</p>
                    {/**@ts-ignore */}
                        <p>Zoom: {zoom && zoom}</p><button onClick={changeDrawCenter}>C</button>
                    </>
                    }
                </div>
                :<p>Show Position Control.</p>
            }
            <div>
            <select name="select" onChange={(e)=>changedMap(e.target.value)}>
                <option value="streets">Calles</option>
                <option value="satellite">Satelite</option>
                <option value="topo">Topografico</option>
            </select>
            <div className="points">                
                <h4>Draw Points Status: {draw?"Active":"Inactive"}{points&&points.length>0&&`, Vertices:${points.length}`}</h4>
                {/**@ts-ignore */}
                <button onClick={changeDraw&&changeDraw} style={{...buttonStyle(draw)}}>O</button>
                {/*@ts-ignore */}
                <button onClick={changeDrawMarker&&changeDrawPoligon&&changeDrawMarker} style={{...buttonStyle(drawMarker)}}>.</button>
                {/*@ts-ignore */}
                <button onClick={changeDrawPoligon&&changeDrawMarker&&changeDrawPoligon} style={{...buttonStyle(drawPoligon)}}>@</button>
            <PointStorage>
            {renderPoints()}
            </PointStorage>
            </div>
            </div>

        </LeftbarS>
    )

}
export default Leftbar