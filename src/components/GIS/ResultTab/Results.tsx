import { useEffect } from "react";
import { Ijurisdiccion, useJurisdiccion } from "../context/JurisdiccionContext";
import { Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { DataGrid, GridColDef } from '@material-ui/data-grid';
import { Grid } from "@material-ui/core";
import { IInteraction, useInteraction } from "../context/InteractionContext";

const columnsRes: GridColDef[] = [
  { field: 'id', 
  headerName: 'ID', 
  width: 150 
},
  { field: 'posicional', 
    headerName: 'Posicional', 
    width: 150 
  },
  { field: 'area', 
  headerName: 'Area', 
  width: 150 
}
]

const columnsHist: GridColDef[] = [
  { field: 'id', 
    headerName: 'ID', 
    width: 120 
  },
  { field: 'dc', 
    headerName: 'DC', 
    width: 150 
  },
  { field: 'municipio', 
  headerName: 'Municipio', 
  width: 150 
},
{ field: 'area', 
  headerName: 'Area', 
  width: 150 
},
]

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    heading: {
      fontSize: theme.typography.pxToRem(15),
      fontWeight: theme.typography.fontWeightRegular,
    },
    accordeon: {
        //@ts-ignore
        disabled: enabler=> enabler.disable
    },
    lista: {
      display: "block"
    },
    cardRoot: {
      width: "100%"
    },
    cardImage: {
      height: 100
    },
  }),
);

function Results() {
    const {
      searchResult, 
      listResultantes, 
      setListResultantes, 
      listHistoricas, 
      setListHistoricas,
      selectedListResultantes, 
      setSelectedListResultantes,
      selectedListHistoricas, 
      setSelectedListHistoricas
    }:Ijurisdiccion = useJurisdiccion() 
    const {setDrawHistoricas, setDrawResultantes}:IInteraction = useInteraction()
    // const [listResultantes, setListResultantes] = useState([])
    // const [listHistoricas, setListHistoricas] = useState([])
    // const [selectedListResultantes, setSelectedListResultantes] = useState([])
    // const [selectedListHistoricas, setSelectedListHistoricas] = useState([])
    const enabler = {
        disabled: (searchResult && searchResult.size > 0)?false:true
    }
  
    const changeDraw = (where: string, list: string[], what: Function)=>{
        if(!searchResult)return
        if (where === "historicas"){
           //@ts-ignore
          setSelectedListHistoricas(list)
        }
        if(where === "resultantes"){
           //@ts-ignore
          setSelectedListResultantes(list)
        }
        let nuevoDraw = list.map((value, idx)=>{
          return searchResult?.get(where)?.get(value)
        })
        what(nuevoDraw)
    }

    
    useEffect(()=>{
      if(!searchResult)return
      const mapResultantes = searchResult?.get("resultantes")
      const mapHistoricas = searchResult?.get("historicas")
      if(!mapResultantes) return
      //@ts-ignore
      let listaRes = []
      //@ts-ignore
      for(let [i, j] of mapResultantes){
        //@ts-ignore
        j.id = i
        //@ts-ignore
        let {id, posicional, area} = j
        //@ts-ignore
        listaRes.push({id, posicional, area})
      }
      //@ts-ignore
      setListResultantes(listaRes)
      if(!mapHistoricas) return
      //@ts-ignore
      let listaHist = []
      //@ts-ignore
      for(let [i, j] of mapHistoricas){
        //@ts-ignore
        j.id = i
        //@ts-ignore
        let {id, dc, municipio, area} = j
        //@ts-ignore
        listaHist.push({id, dc, municipio, area})
      }
      //@ts-ignore
      setListHistoricas(listaHist)
      
    },[searchResult])//eslint-disable-line

    //const listHistoricas = mapHistoricas.map(value=>value)

    const classes = useStyles(enabler)
    return (
        <div className={classes.root}>
        <Accordion className={classes.accordeon}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1-resultantes-content"
            id="panel1-resultantes-header"
          >

            <Typography className={classes.heading}>
                {/* @ts-ignore*/}
                Resultantes: {searchResult? searchResult.get("resultantes")?.size: 0}
            </Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.lista}>
            <Grid container>
                </Grid>
            <div style={{ height: 330, width: '100%' }}>

              {/* @ts-ignore */}
            <DataGrid rows={listResultantes}
              columns={columnsRes}
              rowsPerPageOptions={[5, 10, 25, 50, 100]}
              // pageSize={5}
              checkboxSelection
              disableSelectionOnClick
              //@ts-ignore 
              onSelectionModelChange={(item)=>{changeDraw("resultantes", item, setDrawResultantes)}}
              selectionModel={selectedListResultantes}
              />
              </div>
            <Typography>
              
            </Typography>
          </AccordionDetails>
        </Accordion>
        <Accordion className={classes.accordeon}>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel2a-content"
            id="panel2a-header"
          >
            <Typography className={classes.heading}>
                 {/* @ts-ignore*/}
                 Historicas: {searchResult? searchResult.get("historicas")?.size: 0}
            </Typography>
          </AccordionSummary>
          <AccordionDetails className={classes.lista}>

            <div style={{ height: 340, width: '100%' }}>

              {/* @ts-ignore */}
            <DataGrid rows={listHistoricas}
              columns={columnsHist}
              rowsPerPageOptions={[5, 10, 25, 50, 100]}
              // pageSize={5}
              checkboxSelection
              disableSelectionOnClick
              //@ts-ignore
              onSelectionModelChange={(item)=>{changeDraw("historicas", item, setDrawHistoricas)}}
              selectionModel={selectedListHistoricas}
              />
              </div>
            <Typography>
             
            </Typography>
          </AccordionDetails>
            </Accordion>         
        </div>
    );
}

export default Results;