import { makeStyles, Typography } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import { Theme } from '@material-ui/core/styles';
import { createStyles } from '@material-ui/core/styles';
import {useRouteMatch} from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        "&":{
            textAlign: "center",
        },
        "& > div > img":{
            width: "100%",
            
        }
    }
}))

function Development() {
    const match = useRouteMatch()
    const classes = useStyles()
    const part = match.path.replace("/", "")
    console.log(match.path)
    return (
        <Grid 
        className={classes.root}
        container 
        justifyContent="center"
        alignItems="center" >
            <Grid item xs={12} md={12} xl={3}>
                <Typography variant="h5"  align="center" >
                    The page "{part.length?part:"home"}" is currently under Development
                </Typography>
            <img src={process.env.PUBLIC_URL + "/image/codingprogress.png"} alt="Under Development" />
            </Grid>
        </Grid>
    );
}

export default Development;