import React, { FC } from "react";
import { IGeneral, StateProperty, usePage } from "../../contexts/PageContext"
import { NavBarButton, NavbarControl } from "../../styles/PageComponents.style";
import Icon from "../fundamentals/Icons";

const Navbar:FC = ()=>{
    const {side}: {side?:StateProperty} = usePage()
    const renderNav = (list:Array<IGeneral>)=>{
        let newList = list.map((value)=>{
            return <NavBarButton key={value.name} className="navbar link nav"  to={value.path} {...value}>
                <Icon className="navbar link nav svg" {...value} /><span>{value.name}</span>
            </NavBarButton>
        })
        return newList
    }
    return(
    <NavbarControl className="navbar root">
    {side&&renderNav(side)}
    </NavbarControl>
    )
}
export default Navbar