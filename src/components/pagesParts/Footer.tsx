import React from "react";
import { StateProperty, usePage, IGeneral } from "../../contexts/PageContext"
import { FooterButton, FooterNavControl } from "../../styles/PageComponents.style";
import Icon from "../fundamentals/Icons";

const Footer:React.FC = ()=>{
    const {footer}: {footer?:StateProperty} = usePage()
    const renderFooter = (list:Array<IGeneral>)=>{
        let newList = list.map((value)=>{
            return <FooterButton className="footer nav link" as='a' target="_blank" href={value.path} key={value.name} {...value}>
                <Icon className="footer nav link svg" {...value} />
            </FooterButton>
        })
        return newList 
    }
    return(
        <FooterNavControl className="footer root">
            {footer?renderFooter(footer):null}
        </FooterNavControl>
        )
}

export default Footer