import {Container, Grid, makeStyles } from "@material-ui/core"
import { Link } from "react-router-dom"
import { Button } from "@material-ui/core"
import { CardActions } from "@material-ui/core"
import { createStyles } from "@material-ui/core"
import { Theme } from "@material-ui/core"
import { Card, CardContent, CardHeader, CardMedia, Typography } from "@material-ui/core"
import { red } from "@material-ui/core/colors"
import { useRouteMatch } from "react-router"
import { CenterProvider } from "../../GIS/context/CenterContext"
import { InteractionProvider } from "../../GIS/context/InteractionContext"
import GisC from "../../GIS/GisC"
import { JurisdiccionProvider } from "../../GIS/context/JurisdiccionContext"
import Development from "../../dev_busy/Development"
import ConsultaJI from "../../ConsultaJI/ConsultaJI"

const useStyles = makeStyles((theme: Theme)=> createStyles({
    root:{
        maxWidth: 345,
        margin: "5px"
    },
    media:{
        height: 0,
        paddingTop: "56.25%"
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
      },
    expandOpen: {
        transform: 'rotate(180deg)',
      },
    avatar: {
        backgroundColor: red[500],
      },

}))


export default function Prj(){
    const match = useRouteMatch()
    const classes = useStyles()
    
    //console.log(match.url)
    const render = ()=>{
        if(match.url === "/prj/gis"){
            return(
            <>
            <InteractionProvider>
                <CenterProvider> 
                    <JurisdiccionProvider>
                    <GisC />                  
                    </JurisdiccionProvider>
                </CenterProvider>
            </InteractionProvider>
            </>)
        }
        else if(match.url === "/prj/agm"){
            return(
                <ConsultaJI />
            
            )
        }
        else if(match.url === "/prj/adm"){
            return(
            <Development/>
            )
        }
        else if(match.url === "/prj/inmobiliaria"){
            return(
            <Development/>
            )
        }
        else if(match.url === "/prj"){
            return(
                <Container maxWidth="md">
                <Grid container spacing={1}>
                    <Card className={classes.root}>
                        <CardHeader
                        title="Gis"
                        subheader="GeoSpatial Draw and query Diferents APIs."
                        >
                        </CardHeader>
                        <CardMedia
                        className={classes.media}
                        image={process.env.PUBLIC_URL + "/image/gps1.jpg"}
                        title="Gis"
                        >
                        </CardMedia>
                        <CardContent>
                           <Typography variant="body2" color="textSecondary" component="p">
                            Distinct tools surveying query and proc geo data.
                           </Typography>
                        </CardContent>
                        <CardActions>
                            <Link to={match.path + "/gis"}>
                <Button
                    size="small"
                    color="primary"
                    >
                Enter
                </Button>
                        </Link>      
            </CardActions>
                    </Card>
                    <Card className={classes.root}>
                        <CardHeader
                        title="Inmobiliary"
                        subheader="Sell Point find your way in."
                        >
                        </CardHeader>
                        <CardMedia
                        className={classes.media}
                        image={process.env.PUBLIC_URL + "/image/inmobiliary.jpg"}
                        title="Inmobiliiary"
                        >
                        </CardMedia>
                        <CardContent>
                           <Typography variant="body2" color="textSecondary" component="p">
                           Place where you can sell buy and contact properties 
                           </Typography>
                        </CardContent>
                        <CardActions>
                            <Link to={match.path + "/inmobiliaria"}>
                <Button
                    size="small"
                    color="primary"
                    >
                Enter
                </Button>
                        </Link>      
            </CardActions>
                    </Card>
                    <Card className={classes.root}>
                        <CardHeader
                        title="AGM"
                        subheader="Here you can observe the way of AGM."
                        >
                        </CardHeader>
                        <CardMedia
                        className={classes.media}
                        image={process.env.PUBLIC_URL + "/image/survey.jpg"}
                        title="AGM"
                        >
                        </CardMedia>
                        <CardContent>
                           <Typography variant="body2" color="textSecondary" component="p">
                            Here you can see the tools proccess and the things we do to protect you properties.
                           </Typography>
                        </CardContent>
                        <CardActions>
                            <Link to={match.path + "/agm"}>
                                <Button
                                    size="small"
                                    color="primary"
                                >
                                Enter
                                </Button>
                            </Link>      
                        </CardActions>
                    </Card>
                    <Card className={classes.root}>
                        <CardHeader
                        title="Property Admin"
                        subheader="Administer your property here."
                        >
                        </CardHeader>
                        <CardMedia
                        className={classes.media}
                        image={process.env.PUBLIC_URL + "/image/admin.jpg"}
                        title="Property Admin"
                        >
                        </CardMedia>
                        <CardContent>
                           <Typography variant="body2" color="textSecondary" component="p">
                           Here you can manage your properties.
                           </Typography>
                        </CardContent>
                        <CardActions>
                            <Link to={match.path + "/agm"}>
                                <Button
                                    size="small"
                                    color="primary"
                                >
                                Enter
                                </Button>
                            </Link>      
                        </CardActions>
                    </Card>

                    </Grid>
                        </Container>              
                    )
        }
    }
    return(
        <>
        {render()}
        </>
    )
}