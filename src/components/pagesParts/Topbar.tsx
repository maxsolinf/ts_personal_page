import React, { FC } from "react";
import {AppBar, Button, IconButton, Toolbar, Tooltip, Typography} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import HomeIcon from '@material-ui/icons/Home';
import AllInclusiveIcon from '@material-ui/icons/AllInclusive';
import DeveloperModeIcon from '@material-ui/icons/DeveloperMode';
import GitHubIcon from '@material-ui/icons/GitHub';
import InfoIcon from '@material-ui/icons/Info';
import CodeIcon from '@material-ui/icons/Code';
import { makeStyles } from '@material-ui/core/styles';
import { MenuItem, Menu, withStyles, ListItemIcon, ListItemText  } from "@material-ui/core";
import { Link } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
    menu: {
      ["@media (max-width:600px)"]: {// eslint-disable-line
        "& > .MuiPaper-root" :{
          width: "70vw",
          height: "100vh"
      },
      },
      ["@media (min-width:600px)"]: {// eslint-disable-line
        "& > .MuiPaper-root" :{
          width: "10vw",
          height: "100vh"
      },
      },
      "& > .MuiPaper-root > .MuiList-root":{
        display: "flex",
        flexFlow: "column nowrap",
        height: "96%"
      },
      "& > .MuiPaper-root > .MuiList-root > a:last-child":{
        marginTop: "auto"
      },   
    },
  }));



  const StyledMenuItem = withStyles((theme) => ({
    root: {
      '&:focus, &:hover, &:active': {
        backgroundColor: theme.palette.primary.main,
        '&  .MuiListItemIcon-root, & .MuiListItemText-primary': {
          color: theme.palette.common.white
        },
      },
    },
  }))(MenuItem);


const Topbar:FC = ()=>{
    const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
    const classes = useStyles()
    
   
    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget);
      };
    
      const handleClose = () => {
        setAnchorEl(null);
      };

    return(
        <>
        <div className={classes.root}>
        <AppBar>
            <Toolbar variant='dense'>
                <IconButton className={classes.menuButton} edge='start' aria-label='menu' color="inherit" onClick={handleClick}>
                    <MenuIcon  />
                </IconButton>
                <Typography variant="h6" className={classes.title}>Example</Typography>
                    <Button color='inherit'>Login</Button>
            </Toolbar>
        </AppBar>
        

        </div>
        <Menu id="menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        className={classes.menu}
    >
        <Link className="route-link" style={{textDecoration:'none', color:'inherit'}} to="/" onClick={handleClose}>
            <Tooltip title="Go to Home Page" arrow> 
        <StyledMenuItem>
          <ListItemIcon>
         <HomeIcon fontSize="small" />
          </ListItemIcon>
          <ListItemText>Home</ListItemText>
          </StyledMenuItem>
            </Tooltip>
            </Link>
            <Link to="/intro" style={{textDecoration:'none', color:'inherit'}} onClick={handleClose}>
            <Tooltip title="Go to Home Page" arrow> 
          <StyledMenuItem >
          <ListItemIcon >
         <AllInclusiveIcon fontSize="small"/>
          </ListItemIcon>
          <ListItemText>Intro</ListItemText>
          </StyledMenuItem>
          </Tooltip>
            </Link>
            <Link to="/prj" style={{textDecoration:'none', color:'inherit'}} onClick={handleClose}>
          <StyledMenuItem >
          <ListItemIcon >
         <DeveloperModeIcon fontSize="small"/>
          </ListItemIcon>
          <ListItemText>Projects</ListItemText>
          </StyledMenuItem>
            </Link>
            <Link to="/code" style={{textDecoration:'none', color:'inherit'}} onClick={handleClose}>
          <StyledMenuItem onClick={handleClose}>
          <ListItemIcon >
         <CodeIcon fontSize="small"/>
          </ListItemIcon>
          <ListItemText>Code</ListItemText>
          </StyledMenuItem>
            </Link>
            <Link to="/git" style={{textDecoration:'none', color:'inherit'}} onClick={handleClose}>
          <StyledMenuItem >
          <ListItemIcon >
         <GitHubIcon fontSize="small"/>
          </ListItemIcon>
          <ListItemText>Git</ListItemText>
          </StyledMenuItem>
            </Link>
            <Link to="/about" style={{textDecoration:'none', color:'inherit'}} onClick={handleClose}>
          <StyledMenuItem>
          <ListItemIcon >
         <InfoIcon fontSize="small"/>
          </ListItemIcon>
          <ListItemText>About</ListItemText>
          </StyledMenuItem> 
            </Link>
        </Menu>
     </>
    )
}
export default Topbar