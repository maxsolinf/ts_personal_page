import { createStyles, makeStyles } from "@material-ui/core";
import { Badge } from "@material-ui/core";
import { Theme, AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import React, { useState } from "react";
import { useMessager, IMsg } from "../../contexts/MessagerContext";
import MenuIcon from "@material-ui/icons/Menu";
import NotificationsIcon from '@material-ui/icons/Notifications';
import HomeIcon from "@material-ui/icons/Home";
import GitHubIcon from '@material-ui/icons/GitHub';
import MovieIcon from '@material-ui/icons/Movie';
import CodeIcon from '@material-ui/icons/Code';
import AccountTreeIcon from '@material-ui/icons/AccountTree';
import InfoIcon from '@material-ui/icons/Info';
import Drawer from "@material-ui/core/Drawer";
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText'
import { Link } from "react-router-dom";
import { useEffect } from "react";
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        flexGrow: 1
    },
    menu: {

    },

    button: {
        marginRight: theme.spacing(2)
    },
    text: {
        flexGrow: 1,
    },
    fullList: {
        "@media only screen and (max-width: 600px)": {
            minWidth: "65vw",
        },
        "@media only screen and (min-width: 600px)": {
            minWidth: "17vw",
        },
        "& > ul:last-child > li:last-child":{
            marginTop: "50vh", 
        },
        "& > ul:first-child":{
            marginTop: "60px", 
        },
        "& > ul > li:not(li:first-child), & > ul > li:not(li:last-child)": {
            padding:  "0 5vh"
        },


    },
    listItem:{
        "&:hover":{
            backgroundColor: "hsla(200, 50%, 50%, 0.3)",
            color: "white",
        },
        "&":{
            textDecoration:"none",
            padding: "0",
            height: "5vh",
        },         
    },
    listItemLink:{
        textDecoration:"none",
        width: "100%",
        flexGrow: 1,
        display: "flex",
        justifyContent:"flex-start",
        "& >*": {
            gap: "30px"
        }
        
    },

}))
export default function CustomAppbar() {
    const [menu, setMenu] = useState<boolean>(false)
    const { msg }: { msg?: IMsg[], } = useMessager()
    const theme = useTheme();
    const matches = useMediaQuery(theme.breakpoints.down("sm"));
    const classes = useStyles()


    useEffect(()=>{
        console.log(matches)
    },[matches])
    // eslint-disable-next-line

    const toggleDrawer = (open: boolean) => (
        event: React.KeyboardEvent | React.MouseEvent,
    ) => {
        if (
            event.type === "keydown" &&
            ((event as React.KeyboardEvent).key === 'tab' ||
                (event as React.KeyboardEvent).key === 'shift')
        ) {
            return;
        } setMenu(open)
    };

    const list = () => (
        <div
        onClick={toggleDrawer(false)}
        className={classes.fullList}>
            <List>
                <ListItem className={classes.listItem}>
                <Link to="/" className={classes.listItemLink}>
                    <ListItemIcon>
                        <HomeIcon color="inherit" />
                        <ListItemText>Home</ListItemText>
                    </ListItemIcon>
                </Link>
                </ListItem>
                <ListItem className={classes.listItem}>
                <Link to="/intro" className={classes.listItemLink}>
                    <ListItemIcon>
                        <MovieIcon color="inherit" />
                        <ListItemText>Intro</ListItemText>
                    </ListItemIcon>
                </Link>
                </ListItem >
                <ListItem className={classes.listItem}>
                <Link to="/code" className={classes.listItemLink}>
                    <ListItemIcon >
                        <CodeIcon color="inherit" />
                        <ListItemText>Code</ListItemText>
                    </ListItemIcon>
                </Link>
                </ListItem>
                
                <ListItem className={classes.listItem}>
                <Link to="/prj" className={classes.listItemLink}>
                    <ListItemIcon >
                        <AccountTreeIcon color="inherit" />
                        <ListItemText>Projects</ListItemText>
                    </ListItemIcon>
                </Link>
                </ListItem>
                <ListItem className={classes.listItem}>
                <Link to="/git" className={classes.listItemLink}>
                    <ListItemIcon >
                        <GitHubIcon color="inherit" />
                        <ListItemText>GitHub</ListItemText>
                    </ListItemIcon>
                </Link>
                </ListItem>
                </List>
                <Divider />
                <List>
                <ListItem className={classes.listItem}>
                <Link to="/about" className={classes.listItemLink}>
                    <ListItemIcon >
                        <InfoIcon color="inherit" />
                        <ListItemText>About</ListItemText>
                    </ListItemIcon>
                </Link>
                </ListItem>
            </List>
        </div>

    )

    //const WindowWidth: number = innerWidth
    const preMenu = () => {
        return (
            <AppBar position="sticky" color="primary">
                <Toolbar>
                    <IconButton
                        aria-label="menu button"
                        className={classes.button}
                        color="inherit"
                        onClick={toggleDrawer(true)}
                    >
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.text}>
                        Development
                    </Typography>
                    <IconButton aria-label="show new notifications" color="inherit">
                        <Badge badgeContent={msg && msg.length} color="secondary">
                            <NotificationsIcon />
                        </Badge>
                    </IconButton>
                </Toolbar>
            </AppBar>)
    }
    return (
        <>
            {preMenu()}
            <Drawer anchor='left' open={menu} onClose={toggleDrawer(false)}
            >
                {list()}
            </Drawer>
        </>
    )
}