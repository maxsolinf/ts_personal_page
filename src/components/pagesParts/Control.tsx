import { createStyles, IconButton } from '@material-ui/core';
import { Theme } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import ArrowForwardIcon from '@material-ui/icons/ArrowForward';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import GamepadIcon from '@material-ui/icons/Gamepad';
import ZoomInIcon from '@material-ui/icons/ZoomIn';
import ZoomOutIcon from '@material-ui/icons/ZoomOut';

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {

        borderRadius: "50%",
        width: "105px",
        height: "105px",
        border: "1px solid transparent",
        textAlign: "center",
        borderRightColor: "transparent",
        borderLeftColor: "transparent",
        "& > button": {
            width: "30px",
            height: "40px",
        },
        "& > button:first-child": {
            transform: "rotate(-35deg) translateX(-0.3rem)"
        },
        "& > button:nth-child(2)": {
            transform: "translateY(-0.5rem)"
        },
        "& > button:nth-child(3)": {
            transform: "rotate(35deg)"
        },
        "& > button:nth-child(4)": {
            transform: "translateX(-1rem)"
        },
        "& > button:nth-child(5)": {
            transform: "translateX(2.7rem)"
        },
        "& > button:nth-child(6)": {
            transform: "rotate(-35deg) translateY(1.6rem) translateX(-0.8rem)"
        },
        "& > button:nth-child(7)": {
            transform: "rotate(35deg) translateY(0.1rem) translateX(-1.3rem)"
        },
        "& > button:nth-child(8)": {
            transform: "translateX(-0.8rem)"
        },
        "& > button:nth-child(9)": {
            transform: "translateX(-0.8rem)"
        },
    },
    centerer: {
        transform: "translateY(-4.4rem) translateX(1.95rem)"
    },
    zoomer: {
        display: "flex",
        flexFlow: "column nowrap",
        maxWidth: "3rem",
    }

}))
//@ts-ignore
export function ControlDirection(props) {
    const classes = useStyles()
    return (
        <>
        <div className={classes.root}>
            <IconButton>
                <ArrowUpwardIcon />
            </IconButton>
            <IconButton>
                <ArrowUpwardIcon />
            </IconButton>
            <IconButton>
                <ArrowUpwardIcon />
            </IconButton>
            <IconButton>
                <ArrowBackIcon />
            </IconButton>
            <IconButton>
                <ArrowForwardIcon />
            </IconButton>
            <IconButton>
                <ArrowDownwardIcon />
            </IconButton>
            <IconButton>
                <ArrowDownwardIcon />
            </IconButton>
            <IconButton>
                <ArrowDownwardIcon />
            </IconButton>
        </div>
            <IconButton className={classes.centerer}>
                <GamepadIcon/>
            </IconButton>
            </>
    );
}
//@ts-ignore
export function Zoomer(props){
    const classes = useStyles()
    return(
    <div className={classes.zoomer}>
        <IconButton>
            <ZoomInIcon />
        </IconButton>
        <IconButton>
            <ZoomOutIcon/>
        </IconButton>
    </div>
    )}