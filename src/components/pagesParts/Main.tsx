import { useEffect } from "react";
import { Container } from '@material-ui/core';
import { makeStyles } from "@material-ui/core";
import {useRouteMatch} from 'react-router-dom';
import { useMessager, IMsg } from "../../contexts/MessagerContext";
import Alert from "../fundamentals/AlertMessage";
import Prj from "./prj/Prj";
 
interface IWhat{
    what:string
}

const useStyle = makeStyles({
    root: {
        background: 'linear-gradient(45deg, #333, #999)',
        marginTop: "49px",
        height: "calc(100vh - 49px)"
    }
})



const Main = ({what}:IWhat)=>{
    const {msg, delMessage}:{msg?:IMsg[], delMessage?:Function} = useMessager()
    const classes = useStyle()
    let match = useRouteMatch()
    useEffect(()=>{
        if(!what)return
        console.log(`Page changed to ${what}`)
    },[what])
    return(
        <Container className={classes.root} maxWidth='xl'>

            {(match.path.match(/^\/prj/))? <Prj />:<></>}
            
            {(match.path === '/')?<></>:<></>}
            <div className="main message board">
            {(msg&&delMessage)
            ?msg.map((value, idx, arr)=>(<Alert key={`${value.type} ${idx}`} idf={arr.length}{...value} index={idx} delMessage={delMessage}/>))
            :<></>
        }
        </div>
       
        </Container>
    )
}
export default Main