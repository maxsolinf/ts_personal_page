import { useCallback } from "react"
import { useMemo } from "react"
import { useState } from "react"


const useIterator = (list:any, startIndex:number=0)=>{
    const [i, setI] = useState(startIndex) 

    const next = useCallback(()=>{
        if(i === list.length - 1) return setI(startIndex)
            return setI(i + 1)
    },[i, list, startIndex]);

    const prev = useCallback (()=>{
        if(i===startIndex) return setI(list.length-1)
        return setI(i-1)
    },[i, list, startIndex]);

    
    const value = useMemo(()=>{
        return list[i]
    },[i, list]);

    return {value, next, prev}
}
export default useIterator