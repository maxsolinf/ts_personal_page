import Icon from "./Icons"
import {AlertWraper} from '../../styles/Alert.style';

interface IAlertProps{
    text?:string,
    type?:string,
    delMessage?:Function,
    index?:number,
    idf?:number
}

const Alert = ({text, type, delMessage, index, idf}:IAlertProps)=>{
    const colorMap = new Map()
    colorMap.set('info', {background: "hsla(200, 50%, 50%, 0.2)", color:"blue"})
    colorMap.set('danger', {background: 'hsla(10, 50%, 50%, 0.2)', color: "red"})
    colorMap.set('sucess',{background:'hsla(100, 50%, 50%, 0.2)', color: "green"})

    const style = {...colorMap.get(type)}
    return(
        text && type && delMessage && index?(
            <>
        <AlertWraper onClick={()=>delMessage({index})} className={`message dimissible ${type}`} style={{...style}}>
            <p style={{color:style.color, opacity:1}}> {idf&&idf-1} - {text}
            </p>
            <button onClick={()=>delMessage({index})}>
                <Icon name="close" lib="Cg" icon="Close" color={style.color} className={`alert ${type} svg`}/> 
                </button>
        </AlertWraper></>):(<></>)
    )   
}
export default Alert