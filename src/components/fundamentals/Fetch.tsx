import Spiner from "./Spiner";
import useFetch from "./useFetch";


interface IFetchProps{
    uri: string,
    loadingFallback?: JSX.Element,
    renderError?: JSX.Element
    renderSucess: any,
}

const Fetch = ({
    uri,
    loadingFallback=<Spiner />,
    renderError=<div><p>Something terrible happened</p></div>,
    renderSucess,

}:IFetchProps)=>{
    let {loading, error, data} = useFetch(uri)
    //console.log(data)
    return(
        Object.keys(error).length
        ?renderError
        :loading
        ?loadingFallback
        :Object.keys(data).length
        ?renderSucess(data)
        :<></>
    )
}
export default Fetch