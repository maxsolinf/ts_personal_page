import { useEffect } from "react"
import { useState } from "react"
import { useMessager } from "../../contexts/MessagerContext";


const useFetch = (uri:string)=>{
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState({});
    const [data, setData] = useState({});

    const{setMessage}:{setMessage?:Function} = useMessager()

    useEffect(()=>{
        if(!Object.keys(data).length)return
        if(sessionStorage.getItem(uri))return
        let info = JSON.stringify(data, null, 2);
        sessionStorage.setItem(uri, info);
        setMessage&&setMessage({text:`Info saved in local Storage`, type:"info"})
    },[data, uri]) //eslint-disable-line
 
    useEffect(()=>{
        if(sessionStorage.getItem(uri)){
            let info = JSON.parse(sessionStorage.getItem(uri)|| "")
            setData(info)
            setLoading(false)
            setMessage&&setMessage({text:`Info Retreived from Local`, type:"sucess"})
            return
        }
        if(!uri)return
        console.log(uri)
        setLoading(true)
        fetch(uri)
        .then(resp=>resp.json())
        .then(setData)
        .then(()=>{
            setLoading(false)
           setMessage&&setMessage({text:"Information fetched correctly", type:"info"})    
        })
        .catch(error=>{
            console.log(error)
            setError(error)
        });
    },[uri])//eslint-disable-line
    return {data, error, loading}
}
export default useFetch