import {
    FaHome,
    FaCode,
    FaProjectDiagram,
    FaGitAlt,
    FaRegNewspaper,
    FaFacebook,
    FaTwitter,
    FaLinkedin,
    FaWhatsapp,
    FaGithub,
    FaUser,
    FaArrowCircleRight,
    FaArrowCircleLeft,
    FaQuestionCircle,
} from 'react-icons/fa';
import {SiGmail} from 'react-icons/si';
import {CgClose} from 'react-icons/cg';



//@ts-ignore
const Icon = ({name,lib,icon,color,className})=>{
   const iconsLib = new Map()
   iconsLib.set('Home', FaHome)
   iconsLib.set('Code', FaCode)
   iconsLib.set('ProjectDiagram', FaProjectDiagram)
   iconsLib.set('GitAlt', FaGitAlt)
   iconsLib.set('RegNewspaper', FaRegNewspaper)
   iconsLib.set('Facebook', FaFacebook)
   iconsLib.set('Twitter', FaTwitter)
   iconsLib.set('Linkedin',FaLinkedin)
   iconsLib.set('Whatsapp',FaWhatsapp)
   iconsLib.set('Github', FaGithub)
   iconsLib.set('User',FaUser)
   iconsLib.set('Gmail',SiGmail)
   iconsLib.set('ArrowCircleRight', FaArrowCircleRight)
   iconsLib.set('ArrowCircleLeft', FaArrowCircleLeft)
   iconsLib.set('QuestionCircle', FaQuestionCircle)
   iconsLib.set('Close', CgClose)

   let Icono =iconsLib.get(icon)
    return(
        Icono
        ?<Icono color={color} className={className} name={name}/>
        :<FaQuestionCircle color={color} className={className} name={name} />
    )
    
}
export default Icon