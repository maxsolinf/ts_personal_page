import { SpinerS } from "../../styles/Spiner.style"

const Spiner = ()=>{

    return(
        <div style={{display:"flex", alignItems:"center", justifyItems:"center", alignContent:"center", justifyContent:"center"}}>
            <SpinerS />
        </div>
    )
}
export default Spiner