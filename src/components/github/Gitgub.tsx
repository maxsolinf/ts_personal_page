import {  Container, createStyles, makeStyles, Theme } from "@material-ui/core";
import { Card } from "@material-ui/core";
import { CardContent, CardHeader, CardMedia, Typography } from "@material-ui/core";
import red from "@material-ui/core/colors/red";
import {  useMemo } from "react"
import Fetch from "../fundamentals/Fetch"
import { Repos } from "./Repos"
export interface IData{
    avatar_url:string,
    login:string,
    html_url:string,    
    name: string,
    company:string,
    blog: string,
    location:string,
    created_at:string,
    public_repos: number
} 

const useStyles = makeStyles((theme: Theme)=> createStyles({
    root:{
        maxWidth: 550,
        alignSelf: "center",
        borderRadius: "30px"
    },
    media:{
        height: 0,
        paddingTop: "56.25%",
        borderRadius: "15%"
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
          duration: theme.transitions.duration.shortest,
        }),
      },
    expandOpen: {
        transform: 'rotate(180deg)',
      },
    avatar: {
        backgroundColor: red[500],
      },

}))


const GithubUser = ()=>{
    const uri:string = useMemo(()=>`https://api.github.com/users/maximotejeda`, []);
    const repoUri:string = `https://api.github.com/users/maximotejeda/repos`; 
    const classes = useStyles();
    const renderData = ({
        avatar_url, 
        login, 
        html_url, 
        location, 
        name,
        company,
        created_at,
        public_repos
     }:IData)=>{
        
         return(
             <>
             <Card className={classes.root}>
                 <CardHeader
                    title="GitHub Repos"
                    subheader={name}
                 >
                <Typography
                align="right"
                variant="h1"
                >GitHub Repos</Typography>

                 </CardHeader>
                 <CardMedia
                 className={classes.media}
                 image={avatar_url}
                 title="Personal Photo"
                 >
            {/*<img src={avatar_url} alt="" width="180px"/>*/}

                 </CardMedia>
                 <CardContent>
            
            <Typography 
            align="right"
            variant="body2"
            className="derecho"
            >⏲  {Math.floor((Date.now() - Date.parse(created_at)) / 86400000)} Days 🌞 Since Creation.</Typography>
            <Typography
            align="right"
            variant="body1"
            > 👨‍💼 Developer @ {company}</Typography>
            <Typography
            align="right"
            variant="body2"
            >🇩🇴 {location} 🌎</Typography>
                 </CardContent>
            
             </Card>
        <div className="github info">
            <div className="all">

            </div>
            <div className="izquierdo">
            {/*<p>Public Repos <span>{public_repos}</span></p>*/}
            </div>
            <div className="derecho">

            </div>
        </div>
        </>
    )}


    return(
       <Container maxWidth="sm">
        <Fetch uri={uri} renderSucess={(data:IData)=>renderData(data) } />
        <Repos uri={repoUri}/>
        </Container>
    )
}

export default GithubUser