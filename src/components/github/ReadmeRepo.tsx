import { useState } from "react"
import { useEffect, useMemo, useCallback } from "react";
import Markdown from 'react-markdown';
import Spiner from "../fundamentals/Spiner";


interface ReadmeRepoProps{
    downloadUri: string
}
const ReadmeRepo = ({downloadUri}:ReadmeRepoProps)=>{
    const [loading, setLoading] = useState(true)
    const [error, setError] = useState()
    const [markdown, setMarkdown] = useState("")
    const [rep, setRep] = useState("")
    
    const uri = useMemo(()=>new URL(downloadUri),[downloadUri])

    const changer = useCallback(async()=>{
        let path = uri.pathname.split('/')
        let alterPath = path.splice(0, path.length - 1)
        setRep(alterPath[alterPath.length - 1])
        alterPath.push('readme')
        let readmePath:string = alterPath.join('/')
        uri.pathname = readmePath
        return readmePath
    },[uri])

    const loadMarkdown = useCallback(async()=>{
        setLoading(true)
        if(!uri.pathname.endsWith('readme'))return
        if(sessionStorage.getItem(rep)){
            setMarkdown(sessionStorage.getItem(rep)||"")
            console.log("Retriving Readme from LocalStaorage")
            setLoading(false)
            return
        }
        try{
        const {download_url} = await fetch(uri.href).then(resp=>resp.json())
        console.log(download_url)
        const marker = await fetch(download_url).then(resp=>resp.text())
        //console.log(marker)
        setMarkdown(marker)
        setLoading(false) 
    }
        catch(err){
            setError(error)
            console.log("Ha Ocurrido un error")
            console.log(err)
        }

    },[uri,rep, error])

    useEffect(()=>{
        if(sessionStorage.getItem(rep))return
        if(!markdown.length || !rep.length)return
        sessionStorage.setItem(rep, markdown)
        
    },[markdown]) //eslint-disable-line

    useEffect(()=>{
        if(!downloadUri) return
        changer()
        loadMarkdown()
        //console.log(uri)
    },[uri, downloadUri, changer, rep, loadMarkdown])

    return (
        loading
        ?<Spiner />
        :error
        ?<p>An error has ocurred.</p>
        :markdown
        ?<Markdown children={markdown} includeElementIndex={true}/>
        :<></>
    )

}
export default ReadmeRepo