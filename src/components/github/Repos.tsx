import Fetch from "../fundamentals/Fetch"
import Icon from "../fundamentals/Icons"
import useIterator from "../fundamentals/useIterator"
import ReadmeRepo from "./ReadmeRepo"

interface IRepo{
    name:string,
    downloads_url:string,
    description:string,

}

type RepoList = IRepo[]

interface ISelectedRepoProps {
    data:RepoList
}
interface IRepoProps{
    uri:string,
}

export const SelectedRepo = ({data}:ISelectedRepoProps)=>{
    const {value, next, prev} = useIterator(data)
    let iconNext = {name:"arrowNext", color:"blue", lib:"Fa", icon:"ArrowCircleRight", path: ""}
    let iconPrev = {name:"arrowPrev", color:"blue", lib:"Fa", icon:"ArrowCircleLeft", path: ""}
    return(
        <>
        <div className="github controller">
            <button onClick={()=>prev()} className="github control repo btn prev">
                <Icon {...iconPrev} className="github control repo svg prev" /></button>
            <span className="github control repo title">{value&&value.name}</span>
            <button onClick={()=>next()} className="github control repo btn next">
                <Icon {...iconNext} className="github control repo svg next" /></button>
            </div>
            <div className="github repo markdown">
                <ReadmeRepo downloadUri={value.downloads_url}/>
            </div>
        </>
        )

}

export const Repos = ({uri}:IRepoProps) =>{

    return(
    <Fetch uri={uri} renderSucess={(data:RepoList)=>(<SelectedRepo data={data} />)} />)
}