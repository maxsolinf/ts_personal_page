import { Grid } from "@material-ui/core";
import { createStyles } from "@material-ui/core/styles";
import { Theme } from "@material-ui/core/styles";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme: Theme) => createStyles({
    root: {
        "&":{
            textAlign: "center",
        },
        "& > div > img":{
            width: "100%",
            
        }
    }
}))
function Error404() {
    const classes = useStyles()
    return (
        <Grid className={classes.root} container justifyContent="center" alignContent="center">
            <Grid item xs={12} md={6} xl={3}>
            <img src={process.env.PUBLIC_URL + "/image/error404.png"} alt="error in the route" /> 
            </Grid>
        </Grid>
    );
}

export default Error404;