import { useState } from "react";
import { useEffect } from "react";
import { useContext } from "react";
import { createContext } from "react";
import { ReactElement } from "react-markdown";


const MessagerContext = createContext({})

export const useMessager = ()=>useContext(MessagerContext)
interface iMessageProvider{
    children:React.PropsWithChildren<ReactElement>
}
export interface IMsg{
    text?: string,
    type?: string,
    index?: number,
    map?: Function
}
export type msgProp = IMsg[]|[];

export interface IMsgReturn{
    msg: msgProp,
    setMessage: Function,
    delMessage: Function
    length: Function
}


export const MessageProvider = ({children}:iMessageProvider)=>{
    const [msg, setMsg] = useState<object[]>([])
    
    const setMessage = async ({text, type}:{text:string, type:string})=>{
        setMsg([...msg, {text, type}])
    }

    const delMessage = ({index}:{index:number})=>{
        setMsg(msg.filter((val,idx)=>idx!==index))
        console.log("Eliminado Mensaje " + index)
    }
    useEffect(()=>{
        console.log("Cantidad de mensajes: " + msg.length)
    },[msg])

    return (
        <MessagerContext.Provider value={{msg, setMessage, delMessage}}>
            {children}
        </MessagerContext.Provider>
    )
}