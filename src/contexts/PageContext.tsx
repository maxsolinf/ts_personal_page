import React, { useContext, FC, useEffect } from 'react';
import { createContext, useState } from "react";
import * as json from '../data/data.json'

export interface IGeneral {
    name: string,
    path: string,
    lib: string,
    icon: string,
    color: string
}
export type StateProperty = IGeneral[] | []

const PageContext = createContext({})

export const usePage = ()=> useContext(PageContext)

export const PageProvider:FC =({ children }:React.PropsWithChildren<{}>)=>{
    const [side, setSide] = useState<StateProperty>([]);
    const [topbar, setTopbar] = useState<StateProperty|[]>([]);
    const [footer, setFooter] = useState<StateProperty|[]>([]);
    useEffect(()=>{
        setSide(json.side);
        setTopbar(json.bar)
        setFooter(json.footer)
    },[])

    return(
        <PageContext.Provider value={{side, topbar, footer }}>
            {children}
        </PageContext.Provider>
    )
}