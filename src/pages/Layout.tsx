import { Route, Switch } from "react-router";
import Development from "../components/dev_busy/Development";
import Error404 from "../components/error/Error404";
import GithubUser from "../components/github/Gitgub";
import Bar from "../components/pagesParts/NewTopbar";
import Prj from "../components/pagesParts/prj/Prj";
export default function Layout () {
    return( 
        <>
        <Bar/>
        <Switch>
            <Route path="/user">
            <Development />
            </Route>
            <Route path="/intro">
            <Development />
            </Route>
            <Route path="/prj/gis">
            <Prj />
            </Route>
            <Route path="/prj/agm">
            <Prj />
            </Route>
            <Route path="/prj/inmobiliaria">
            <Prj />
            </Route>
            <Route path="/prj">
            < Prj />
            </Route>
            <Route path="/git">
                <GithubUser />
            </Route>
            <Route path="/code">
            <Development />
            </Route>
            <Route path="/about">
            <Development />
            </Route>
            <Route exact path="/">
                <Development />
            </Route>
            <Route path="*">
                <Error404 />
            </Route>
        </Switch>
        </>
    )
}